## LaserApp


### Setup
1. Clone
2. Run `git submodule update --init --recursive`
3. Create/Edit the local.properties to include the NDK path
```
sdk.dir=***
ndk.dir=***
```
4. Build the project (might fail)
5. Go to `libraries/humla/libs/humla-spongycastle` and run `../../gradlew jar`
6. Build again, should woork now


### Configuration
Edit the `gradle.properties` to adjust server domains/ips/ports

#### Signing
You can build and sign staging and release APKs either using android studio or using gradle
To be able to sign the apk using gradle you will need the keystore with the key and put the following config to your user `GRADLE_USER_HOME/gradle.properties` (might be `~/.gradle/gradle.properties`)

```
LASERAPP_CERT_STORE_PATH=/path/to/keeystore
LASERAPP_CERT_STORE_PASS=secret
LASERAPP_RELEASE_KEY_PASS=secret
LASERAPP_STAGING_KEY_PASS=secret
```