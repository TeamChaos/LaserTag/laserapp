package de.teamchaos.laserapp;

/**
 * LaserApp
 *
 * @author maxanier
 */
public class REFERENCE {
    public static final String PREFS = "general";
    public static final String PREF_MUTE = "sound_effect_mute";

    public static final String PREF_PUSH_TO_TALK = "push_to_talk";
}
