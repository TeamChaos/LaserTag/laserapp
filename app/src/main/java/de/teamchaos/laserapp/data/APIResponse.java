package de.teamchaos.laserapp.data;

import androidx.annotation.Nullable;
import com.google.gson.*;

import javax.annotation.Nonnull;
import java.lang.reflect.Type;
import java.util.Optional;


/**
 * API response data https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/api_response.schema.json
 *
 * @param <T> The type of the additional payload. Depends on what has been requested from the api
 */
public class APIResponse<T> {
    private STATUS status;
    private String error;
    @Nullable
    private T data;

    /**
     * @return If provided, the additional payload object
     */
    @Nonnull
    public Optional<T> getData() {
        return Optional.ofNullable(data);
    }

    public STATUS getStatus() {
        return status;
    }

    public boolean isSuccess() {
        return status == STATUS.SUCCESS;
    }

    /**
     * Server API response status code
     */
    public enum STATUS {
        SUCCESS(0), BAD_TIME(1), BAD_REQUEST(2), ERROR(3), UNAUTHORIZED(4);

        private final int id;

        STATUS(int id) {
            this.id = id;
        }


    }

    static class Deserializer implements JsonDeserializer<APIResponse<?>> {

        private final Gson gson;

        public Deserializer(Gson gson) {
            this.gson = gson;
        }

        @Override
        public APIResponse<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject data = json.getAsJsonObject();
            APIResponse response = new APIResponse<>();
            int resultID = data.get("result").getAsInt();
            if (resultID < 0 || resultID >= STATUS.values().length) {
                throw new InvalidResponseException("Invalid result id");
            }
            response.status = STATUS.values()[resultID];
            if (response.status != STATUS.SUCCESS) {
                response.error = data.has("error") ? data.get("error").getAsString() : "Unknown";
                return response;
            }
            if (data.has("status")) {
                response.data = gson.fromJson(data.get("status"), GameStatus.class);
            } else if (data.has("config")) {
                response.data = gson.fromJson(data.get("config"), GameConfig.class);
            } else if (data.has("scoreboard")) {
                response.data = gson.fromJson(data.get("scoreboard"), Scoreboard.class);
            } else if (data.has("voiceserver")) {
                response.data = gson.fromJson(data.get("voiceserver"), VoiceServerInfo.class);
            }
            return response;

        }
    }

}
