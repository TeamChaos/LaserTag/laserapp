package de.teamchaos.laserapp.data;

import androidx.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializer;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.bind.TreeTypeAdapter;
import com.google.gson.reflect.TypeToken;


/**
 * Provide the appropriate GSON deserializer for each data class
 */
@SuppressWarnings("unchecked")
public class AdapterFactory implements TypeAdapterFactory {

    @Nullable
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        JsonDeserializer<T> deserializer = null;
        if (APIResponse.class.isAssignableFrom(type.getRawType())) {
            deserializer = (JsonDeserializer<T>) new APIResponse.Deserializer(gson);
        } else if (GameSetup.class.isAssignableFrom(type.getRawType())) {
            deserializer = (JsonDeserializer<T>) new GameSetup.Deserializer(gson);
        } else if (PlayerID.class.isAssignableFrom(type.getRawType())) {
            deserializer = (JsonDeserializer<T>) new PlayerID.Deserializer();
        } else if (WSResponse.class.isAssignableFrom(type.getRawType())) {
            deserializer = (JsonDeserializer<T>) new WSResponse.Deserializer(gson);
        }
        if (deserializer != null) {
            return new TreeTypeAdapter<>(null, deserializer, gson, type, this);
        }

        return null;
    }
}
