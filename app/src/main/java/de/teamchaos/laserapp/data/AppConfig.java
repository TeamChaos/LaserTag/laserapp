package de.teamchaos.laserapp.data;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;

import java.util.Optional;

/**
 * App config
 */
public class AppConfig implements Parcelable {

    public static final Creator<AppConfig> CREATOR = new Creator<AppConfig>() {
        @Override
        public AppConfig createFromParcel(Parcel in) {
            return new AppConfig(in);
        }

        @Override
        public AppConfig[] newArray(int size) {
            return new AppConfig[size];
        }
    };
    private String server;
    private int port;
    private String credentials;
    private boolean secure;
    @Nullable
    private PlayerID playerid;


    public AppConfig(String server, int port, boolean secure, String credentials, @Nullable PlayerID playerid) {
        this.server = server;
        this.port = port;
        this.credentials = credentials;
        this.secure = secure;
        this.playerid = playerid;
    }

    protected AppConfig() {

    }

    @Override
    public String toString() {
        return "AppConfig{" +
                "server='" + server + '\'' +
                ", port=" + port +
                ", credentials='" + credentials + '\'' +
                ", secure=" + secure +
                ", playerid=" + playerid +
                '}';
    }

    protected AppConfig(Parcel in) {
        server = in.readString();
        port = in.readInt();
        secure = in.readInt() != 0;
        int id = in.readInt();
        playerid = id == -1 ? null : new PlayerID(id);
        credentials = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getCredentials() {
        return credentials;
    }

    /**
     * @return If not present, behave as a general observer
     */
    public Optional<PlayerID> getPlayerid() {
        return Optional.of(playerid);
    }

    public int getPort() {
        return port;
    }

    public String getServer() {
        return server;
    }

    public boolean isSecure() {
        return secure;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(server);
        dest.writeInt(port);
        dest.writeInt(secure ? 1 : 0);
        dest.writeInt(playerid == null ? -1 : playerid.getId());
        dest.writeString(credentials);
    }

}
