package de.teamchaos.laserapp.data;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;


/**
 * Game configuration https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/game_config.schema.json
 */
public class GameConfig {
    private GameSetup setup;
    private int scoreLimit;

    public int getScoreLimit() {
        return scoreLimit;
    }

    public STOP_TRIGGER getStopTrigger() {
        return stopTrigger;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    private int timeLimit;
    private STOP_TRIGGER stopTrigger;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameConfig that = (GameConfig) o;
        return scoreLimit == that.scoreLimit &&
                timeLimit == that.timeLimit &&
                setup.equals(that.setup) &&
                stopTrigger == that.stopTrigger;
    }

    public GameSetup getSetup() {
        return setup;
    }

    @Override
    public int hashCode() {
        return Objects.hash(setup, scoreLimit, timeLimit, stopTrigger);
    }

    /**
     * Which trigger causes the game to stop
     */
    public enum STOP_TRIGGER {
        @SerializedName("time") TIME, @SerializedName("score") SCORE, @SerializedName("manual") MANUAL
    }
}
