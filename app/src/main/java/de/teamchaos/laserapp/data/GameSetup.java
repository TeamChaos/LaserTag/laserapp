package de.teamchaos.laserapp.data;


import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * Game setup with the information everybody has https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/game_setup.schema.json
 */
public class GameSetup {
    private GAME_MODE gameMode;
    /**
     * Map team id to Team
     */
    private Int2ObjectMap<Team> teams;
    private boolean friendlyFire;
    /**
     * Map player id to Player
     */
    private Int2ObjectMap<Player> players;
    private LAUNCH_TRIGGER launchTrigger;
    private int launchTime;
    private int killMultiplier;
    private int modePoints;
    private int assistMultiplier;
    private boolean localRespawn;
    private int respawnOffset;
    private int respawnDelay;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameSetup gameSetup = (GameSetup) o;
        return friendlyFire == gameSetup.friendlyFire &&
                killMultiplier == gameSetup.killMultiplier &&
                modePoints == gameSetup.modePoints &&
                assistMultiplier == gameSetup.assistMultiplier &&
                localRespawn == gameSetup.localRespawn &&
                respawnOffset == gameSetup.respawnOffset &&
                respawnDelay == gameSetup.respawnDelay &&
                gameMode == gameSetup.gameMode &&
                teams.equals(gameSetup.teams) &&
                players.equals(gameSetup.players) &&
                launchTrigger == gameSetup.launchTrigger && launchTime == gameSetup.launchTime;
    }

    public int getAssistMultiplier() {
        return assistMultiplier;
    }

    public GAME_MODE getGameMode() {
        return gameMode;
    }

    public int getKillMultiplier() {
        return killMultiplier;
    }

    public int getLaunchTime() {
        return launchTime;
    }

    public LAUNCH_TRIGGER getLaunchTrigger() {
        return launchTrigger;
    }

    public int getModePoints() {
        return modePoints;
    }

    public Int2ObjectMap<Player> getPlayers() {
        return Int2ObjectMaps.unmodifiable(players);
    }

    public int getRespawnDelay() {
        return respawnDelay;
    }

    public int getRespawnOffset() {
        return respawnOffset;
    }

    public Int2ObjectMap<Team> getTeams() {
        return Int2ObjectMaps.unmodifiable(teams);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameMode, teams, friendlyFire, players, launchTrigger, killMultiplier, modePoints, assistMultiplier, localRespawn, respawnOffset, respawnDelay);
    }

    public boolean isFriendlyFire() {
        return friendlyFire;
    }

    public boolean isLocalRespawn() {
        return localRespawn;
    }

    public enum GAME_MODE {
        @SerializedName("FreeForAll") FREE_FOR_ALL(false), @SerializedName("TeamDeathMatch") TEAM_DEATH_MATCH(true), @SerializedName("Domination") DOMINATION(true), @SerializedName("Conquest") CONQUEST(true);

        public final boolean isTeamBased;

        GAME_MODE(boolean isTeamBased) {
            this.isTeamBased = isTeamBased;
        }
    }

    public enum LAUNCH_TRIGGER {
        @SerializedName("time") TIME, @SerializedName("spawnpoint") SPAWNPOINT
    }

    /**
     * Team data: Name + id
     */
    public static class Team {
        private String name;
        private int id;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Team team = (Team) o;
            return id == team.id &&
                    name.equals(team.name);
        }

        public int getID() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, id);
        }
    }

    /**
     * Player data: Name + id + Team
     */
    public static class Player {
        private String name;
        private int id;
        private int team;

        public Player(int id, int team, String name) {
            this.id = id;
            this.team = team;
            this.name = name;
        }

        public Player() {

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Player player = (Player) o;
            return id == player.id &&
                    team == player.team &&
                    name.equals(player.name);
        }

        public int getID() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getTeam() {
            return team;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, id, team);
        }
    }

    static class Deserializer implements JsonDeserializer<GameSetup> {

        private final Gson gson;

        public Deserializer(Gson gson) {
            this.gson = gson;
        }

        @Override
        public GameSetup deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject data = json.getAsJsonObject();
            GameSetup setup = new GameSetup();
            setup.gameMode = gson.fromJson(data.get("gameMode"), GAME_MODE.class);
            setup.friendlyFire = data.get("friendlyFire").getAsBoolean();
            setup.launchTrigger = gson.fromJson(data.get("launchTrigger"), LAUNCH_TRIGGER.class);
            setup.killMultiplier = data.get("killMultiplier").getAsInt();
            setup.modePoints = data.get("modePoints").getAsInt();
            setup.assistMultiplier = data.get("assistMultiplier").getAsInt();
            setup.localRespawn = data.get("localRespawn").getAsBoolean();
            setup.respawnOffset = data.get("respawnOffset").getAsInt();
            setup.respawnDelay = data.get("respawnDelay").getAsInt();
            setup.launchTime = data.get("launchTime").getAsInt();
            JsonObject t = data.getAsJsonObject("teams");
            Int2ObjectMap<Team> teams = new Int2ObjectArrayMap<>();
            t.entrySet().forEach(e -> {
                int id = Integer.parseInt(e.getKey());
                Team team = gson.fromJson(e.getValue(), Team.class);
                if (id != team.id) throw new InvalidResponseException("Team ID must be equal to object property name");
                teams.put(id, team);
            });
            if (teams.size() == 0) {
                throw new JsonParseException("Must provide at least one team");
            }
            setup.teams = teams;
            JsonObject p = data.getAsJsonObject("players");
            Int2ObjectMap<Player> players = new Int2ObjectArrayMap<>();
            p.entrySet().forEach(e -> {
                int id = Integer.parseInt(e.getKey());
                Player player = gson.fromJson(e.getValue(), Player.class);
                if (id != player.id)
                    throw new InvalidResponseException("Player ID must be equal to object property name");
                if (!teams.containsKey(player.getTeam())) throw new InvalidResponseException("Player team must exist");
                players.put(id, player);
            });
            if (players.size() == 0) {
                throw new InvalidResponseException("Must provide at least one player");
            }
            setup.players = players;


            return setup;
        }
    }
}
