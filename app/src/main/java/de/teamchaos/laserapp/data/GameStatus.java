package de.teamchaos.laserapp.data;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.Objects;


/**
 * The current status of the game https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/game_status.schema.json
 */
public class GameStatus {
    public State state;
    private int[] players;
    private int[] tobeready;
    private int[] tobeevaluated;
    private int timer;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameStatus that = (GameStatus) o;
        return timer == that.timer &&
                state == that.state &&
                Arrays.equals(players, that.players) &&
                Arrays.equals(tobeready, that.tobeready) &&
                Arrays.equals(tobeevaluated, that.tobeevaluated);
    }

    public int getTimer() {
        return timer;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(state, timer);
        result = 31 * result + Arrays.hashCode(players);
        result = 31 * result + Arrays.hashCode(tobeready);
        result = 31 * result + Arrays.hashCode(tobeevaluated);
        return result;
    }

    /**
     * Game state
     */
    public enum State {
        @SerializedName("setup") SETUP, @SerializedName("ready") READY, @SerializedName("starting") STARTING, @SerializedName("running") RUNNING, @SerializedName("evaluation") EVALUATION, @SerializedName("finished") FINISHED
    }
}
