package de.teamchaos.laserapp.data;

import com.google.gson.JsonParseException;


/**
 * Thrown if something in a response/payload is not as expected/does not obey the rules
 */
public class InvalidResponseException extends JsonParseException {
    public InvalidResponseException(String msg) {
        super(msg);
    }
}
