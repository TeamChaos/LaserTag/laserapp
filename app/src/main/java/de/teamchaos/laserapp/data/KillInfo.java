package de.teamchaos.laserapp.data;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Described a single kill event
 */
public class KillInfo {
    public List<PlayerID> getAssists() {
        return Collections.unmodifiableList(Arrays.asList(assists));
    }

    public PlayerID getOffenderID() {
        return offenderID;
    }

    public PlayerID getVictimID() {
        return victimID;
    }

    private PlayerID offenderID;
    private PlayerID victimID;
    private PlayerID[] assists;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KillInfo killInfo = (KillInfo) o;
        return offenderID.equals(killInfo.offenderID) &&
                victimID.equals(killInfo.victimID) &&
                Arrays.equals(assists, killInfo.assists);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(offenderID, victimID);
        result = 31 * result + Arrays.hashCode(assists);
        return result;
    }
}
