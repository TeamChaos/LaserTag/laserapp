package de.teamchaos.laserapp.data;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Objects;


/**
 * Wrapper class for player id
 */
public class PlayerID {
    private int id;

    private PlayerID() {

    }

    public PlayerID(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerID playerID = (PlayerID) o;
        return id == playerID.id;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    static class Deserializer implements JsonDeserializer<PlayerID> {

        @Override
        public PlayerID deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            int id = json.getAsInt();
            if (id < 0 || id > 9) {
                throw new InvalidResponseException("Invalid id");
            }
            return new PlayerID(id);
        }
    }
}
