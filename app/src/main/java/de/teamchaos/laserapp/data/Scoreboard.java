package de.teamchaos.laserapp.data;


import java.util.Arrays;
import java.util.Objects;

/**
 * Collected but unprocessed scoreboard data https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/scoreboard.schema.json
 */
public class Scoreboard {

    private boolean team;
    private ScoreboardEntryData[] score;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scoreboard that = (Scoreboard) o;
        return team == that.team &&
                Arrays.equals(score, that.score);
    }

    public ScoreboardEntryData[] getScore() {
        return score;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(team);
        result = 31 * result + Arrays.hashCode(score);
        return result;
    }

    /**
     * Scoreboard entry for a single player
     */
    public static class ScoreboardEntryData extends GameSetup.Player implements Comparable<ScoreboardEntryData> {

        private int kills;
        private int assists;
        private int deaths;
        private int score;

        public ScoreboardEntryData(int id, int team, String name) {
            super(id, team, name);
        }

        public ScoreboardEntryData() {
            super();
        }

        @Override
        public int compareTo(ScoreboardEntryData o) {
            return o.score - this.score;
        }

        public void copyStatsFrom(ScoreboardEntryData source) {
            this.kills = source.kills;
            this.assists = source.assists;
            this.deaths = source.deaths;
            this.score = source.score;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            ScoreboardEntryData that = (ScoreboardEntryData) o;
            return kills == that.kills &&
                    assists == that.assists &&
                    deaths == that.deaths &&
                    score == that.score;
        }

        public int getAssists() {
            return assists;
        }

        public int getDeaths() {
            return deaths;
        }

        public int getKills() {
            return kills;
        }

        public int getScore() {
            return score;
        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), kills, assists, deaths, score);
        }

    }
}

