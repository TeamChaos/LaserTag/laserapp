package de.teamchaos.laserapp.data;

/**
 * Voice server info https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/voiceserver_info.schema.json
 */
public class VoiceServerInfo {
    private boolean available;
    private String host;
    private int port;
    private String password;

    public String getHost() {
        return host;
    }

    public String getPassword() {
        return password;
    }

    public int getPort() {
        return port;
    }

    public boolean isAvailable() {
        return available;
    }
}
