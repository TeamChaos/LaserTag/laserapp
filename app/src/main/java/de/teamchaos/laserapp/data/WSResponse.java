package de.teamchaos.laserapp.data;


import androidx.annotation.Nullable;
import com.google.common.base.Preconditions;
import com.google.gson.*;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

/**
 * Data class for messages sent by the server via websocket https://gitlab.com/TeamChaos/LaserTag/baseserver/-/blob/master/schemas/app_ws_message.schema.json
 */
public class WSResponse {
    private Event event;
    @Nullable
    private Object data;

    public GameConfig getConfig() {
        Preconditions.checkState(event.payload == GameConfig.class, "Cannot get config from a non config event");
        return (GameConfig) data;
    }

    public Event getEvent() {
        return event;
    }

    public KillInfo getKillInfo() {
        Preconditions.checkState(event.payload == KillInfo.class, "Cannot get killinfo from non kill event");
        return (KillInfo) data;
    }

    public PlayerID[] getPlayerList() {
        Preconditions.checkState(event.payload == PlayerID[].class, "Cannot get players from a non player id array event");
        return (PlayerID[]) data;
    }

    public Scoreboard getScoreboard() {
        //Probably completely unnecessary to have the classes in the enum. Could just to instanceof
        Preconditions.checkState(event.payload == Scoreboard.class, "Cannot get scoreboard from a non scoreboard event");
        return (Scoreboard) data;
    }

    /**
     * Websocket game event
     */
    public enum Event {
        @SerializedName("config_changed") CONFIG_CHANGED(GameConfig.class), @SerializedName("players_changed") PLAYERS_CHANGED(PlayerID[].class), @SerializedName("players_tobeevaluated_changed") PLAYERS_EVALUATE_CHANGED(PlayerID[].class), @SerializedName("players_tobeready_changed") PLAYERS_READY_CHANGED(PlayerID[].class), @SerializedName("game_starting") GAME_STARTING(null), @SerializedName("game_started") GAME_STARTED(null), @SerializedName("game_ready") GAME_READY(GameConfig.class), @SerializedName("game_stopped") GAME_STOPPED(null), @SerializedName("kill") KILL(KillInfo.class), @SerializedName("scoreboard") SCOREBOARD(Scoreboard.class), @SerializedName("game_finished") GAME_FINISHED(Scoreboard.class), @SerializedName("reset") RESET(null);

        /**
         * Expected class of the payload
         */
        @Nullable
        private final Class<?> payload;

        Event(@Nullable Class<?> payload) {
            this.payload = payload;
        }
    }

    static class Deserializer implements JsonDeserializer<WSResponse> {

        private final Gson gson;

        Deserializer(Gson gson) {
            this.gson = gson;
        }

        @Override
        public WSResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject data = json.getAsJsonObject();
            WSResponse response = new WSResponse();
            response.event = gson.fromJson(data.get("event"), Event.class);
            switch (response.event) {
                case GAME_FINISHED:
                case SCOREBOARD:
                    response.data = gson.fromJson(data.get("scoreboard"), Scoreboard.class);
                    break;
                case GAME_READY:
                case CONFIG_CHANGED:
                    response.data = gson.fromJson(data.get("config"), GameConfig.class);
                    break;
                case PLAYERS_CHANGED:
                case PLAYERS_READY_CHANGED:
                case PLAYERS_EVALUATE_CHANGED:
                    response.data = gson.fromJson(data.get("list"), PlayerID[].class);
                    break;
                case KILL:
                    response.data = gson.fromJson(data.get("kill"), KillInfo.class);
                    break;
            }
            return response;
        }
    }
}
