package de.teamchaos.laserapp.service;

import de.teamchaos.laserapp.data.AppConfig;
import de.teamchaos.laserapp.data.GameConfig;
import de.teamchaos.laserapp.data.GameStatus;
import de.teamchaos.laserapp.data.Scoreboard;
import de.teamchaos.laserapp.service.game.GameStateHandler;
import de.teamchaos.laserapp.service.map.ILocationHandler;
import de.teamchaos.laserapp.service.network.IAppWebsocketHandler;
import de.teamchaos.laserapp.service.sound.ISoundEffectPlayer;

import java.util.function.Consumer;

/**
 * Handles the connection to the server and all related functionality that has to run in the background
 */
public interface IServerConnectionService {

    GameStateHandler getGameStateHandler();

    ILocationHandler getLocationHandler();

    /**
     * @return The id of the player running this app
     */
    int getPlayerID();

    ISoundEffectPlayer getSoundEffectPlayer();

    IAppWebsocketHandler getWebsocketHandler();

    /**
     * @return Whether connected to the websocket server
     */
    boolean isConnected();

    boolean isVoiceEnabled();

    void onRuntimePermissionsGranted();

    /**
     * @return Use this reference to cancel the request if necessary
     */
    ICancelableRequest requestConfig(Consumer<GameConfig> onSuccess);

    /**
     * @return Use this reference to cancel the request if necessary
     */
    ICancelableRequest requestScoreboard(Consumer<Scoreboard> onSuccess);

    /**
     * @return Use this reference to cancel the request if necessary
     */
    ICancelableRequest requestStatusUpdate(Consumer<GameStatus> onSuccess);

    boolean sendMessage(String msg);

    void stop();

    AppConfig getAppConfig();


    interface ICancelableRequest {
        void cancel();
    }
}
