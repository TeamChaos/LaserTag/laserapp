package de.teamchaos.laserapp.service;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.REFERENCE;
import de.teamchaos.laserapp.data.*;
import de.teamchaos.laserapp.service.game.GameStateHandler;
import de.teamchaos.laserapp.service.map.ILocationHandler;
import de.teamchaos.laserapp.service.map.LocationHandler;
import de.teamchaos.laserapp.service.network.AppWebsocketHandler;
import de.teamchaos.laserapp.service.network.GsonRequest;
import de.teamchaos.laserapp.service.network.IAppWebsocketHandler;
import de.teamchaos.laserapp.service.sound.ISoundEffectPlayer;
import de.teamchaos.laserapp.service.sound.SoundEffectPlayer;
import de.teamchaos.laserapp.service.voice.VoiceChatHandler;
import de.teamchaos.laserapp.ui.MainActivity;
import de.teamchaos.laserapp.ui.WaitingActivity;
import de.teamchaos.laserapp.ui.game.GameActivity;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * This service should be running as a foreground service at all times
 */
public class ServerConnectionService extends Service implements AppWebsocketHandler.IAppWebsocketListener, IServerConnectionService, SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String START_INTENT_CONFIG = "config";
    private final static String TAG = "ServerConnection";
    private static ServerConnectionService INSTANCE;

    public static Optional<IServerConnectionService> getInstance() {
        return Optional.ofNullable(INSTANCE);
    }

    /**
     * Returns the intent for the activity which should be active at the current state.
     * If you pass an intent of the currently active activity, it will return an empty optional if it is already the correct one.
     * If you pass null, the optional will never be empty
     *
     * @param active optional, the intent of the currently active activity
     */
    public static Optional<Intent> getIntentForState(Context context, @Nullable Intent active) {
        if (INSTANCE == null) {
            return Optional.of(new Intent(context, MainActivity.class));
        }
        if (!INSTANCE.isConnected()) {
            Intent intent = new Intent(context, WaitingActivity.class);
            intent.putExtra(WaitingActivity.WAITING_STRING_ID_ARG, R.string.waiting_for_connection);
            return ensureDifferentIntent(intent, active);
        } else if (INSTANCE.gameStateHandler.getCurrentState() == GameStatus.State.SETUP) {
            Intent intent = new Intent(context, WaitingActivity.class);
            intent.putExtra(WaitingActivity.WAITING_STRING_ID_ARG, R.string.waiting_for_game_setup);
            return ensureDifferentIntent(intent, active);
        } else {
            return ensureDifferentIntent(new Intent(context, GameActivity.class), active);
        }
    }

    /**
     * If both intents are provided, this returns either an optional of the first intent, if they are different, or an empty optional, if they are identical.
     *
     * @param oldIntent if null, this will always return an optional of the first intent
     */
    private static Optional<Intent> ensureDifferentIntent(@Nonnull Intent newIntent, @Nullable Intent oldIntent) {
        if (oldIntent == null) return Optional.of(newIntent);
        if (oldIntent.getComponent().getClassName().equals(newIntent.getComponent().getClassName())) {
            Bundle oldBundle = oldIntent.getExtras();
            if (oldBundle == null) return newIntent.getExtras() == null ? Optional.empty() : Optional.of(newIntent);
            for (String s : oldBundle.keySet()) {
                if (!Objects.equals(oldBundle.get(s), newIntent.getExtras().get(s))) {
                    return Optional.of(newIntent);
                }
            }
            return Optional.empty();
        }
        return Optional.of(newIntent);
    }

    /**
     * @return an intent describing the activity that should be running according to the current state of the game/app
     */
    public static Intent getIntentForState(Context context) {
        //noinspection OptionalGetWithoutIsPresent
        return getIntentForState(context, null).get();
    }

    private final int NOTIFICATION_ID = R.string.server_connection_service;
    private final String NOTIFICATION_CHANNEL_ID = "server";
    private NotificationManager notificationManager;
    private AppWebsocketHandler websocketHandler;
    private AppConfig appConfig;
    private GameStateHandler gameStateHandler;
    private LocationHandler locationHandler;
    private SoundEffectPlayer soundEffectPlayer;
    private VoiceChatHandler voiceChatHandler;


    private boolean isVoiceEnabled;
    /**
     * If the current status should be requested in its entirety via the REST API
     */
    private boolean requestStatusUpdate = true;
    private RequestQueue requestQueue;

    @Override
    public GameStateHandler getGameStateHandler() {
        return gameStateHandler;
    }

    @Override
    public ILocationHandler getLocationHandler() {
        return locationHandler;
    }

    @Override
    public int getPlayerID() {
        return this.appConfig.getPlayerid().map(PlayerID::getId).orElse(-1);
    }

    @Override
    public ISoundEffectPlayer getSoundEffectPlayer() {
        return this.soundEffectPlayer;
    }

    @Override
    public IAppWebsocketHandler getWebsocketHandler() {
        return this.websocketHandler;
    }

    @Override
    public boolean isConnected() {
        return websocketHandler.isWebsocketConnected();
    }

    @Override
    public boolean isVoiceEnabled() {
        return isVoiceEnabled;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        notificationManager = getSystemService(NotificationManager.class);
        gameStateHandler = new GameStateHandler(this);
        websocketHandler = new AppWebsocketHandler(this);
        websocketHandler.registerListener(this);
        websocketHandler.registerListener(gameStateHandler);
        locationHandler = new LocationHandler(this);
        gameStateHandler.registerListener(locationHandler);
        soundEffectPlayer = new SoundEffectPlayer();
        voiceChatHandler = new VoiceChatHandler(this);

        showNotification();
        setupRequestQueue();
        INSTANCE = this;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        notificationManager.cancel(NOTIFICATION_ID);
        voiceChatHandler.stop();
        isVoiceEnabled = false;

        // Tell the user we stopped.
        Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
        INSTANCE = null;
    }

    @Override
    public void onRuntimePermissionsGranted() {
        locationHandler.startTrackingLocation(gameStateHandler.getCurrentState() == GameStatus.State.RUNNING);
        if (appConfig.getPlayerid().isPresent()) requestVoiceserverInfo(info -> {
            if (info.isAvailable()) {
                voiceChatHandler.start(info, appConfig.getPlayerid().get().getId(), getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).getBoolean(REFERENCE.PREF_PUSH_TO_TALK, true));
                isVoiceEnabled = true;
            }
        });
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (REFERENCE.PREF_MUTE.equals(key)) {
            this.soundEffectPlayer.setMuted(sharedPreferences.getBoolean(key, false));
        } else if (REFERENCE.PREF_PUSH_TO_TALK.equals(key)) {
            if (isVoiceEnabled) voiceChatHandler.changeInputMode(sharedPreferences.getBoolean(key, true));
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);
        this.appConfig = intent.getParcelableExtra(START_INTENT_CONFIG);
        if (appConfig == null || !appConfig.getPlayerid().isPresent())
            throw new IllegalArgumentException("Must provide player id and secret"); //TODO make app work without player id

        SharedPreferences prefs = getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE);
        prefs.registerOnSharedPreferenceChangeListener(this);

        websocketHandler.setup(appConfig);
        websocketHandler.checkWebsocket();

        locationHandler.startTrackingLocation(false);
        requestVoiceserverInfo(info -> {
            if (info.isAvailable()) {
                voiceChatHandler.start(info, getPlayerID(), prefs.getBoolean(REFERENCE.PREF_PUSH_TO_TALK, true));
                isVoiceEnabled = true;
            }
        });
        soundEffectPlayer.loadSounds(this);
        gameStateHandler.configure(soundEffectPlayer, appConfig.getPlayerid().get());
        soundEffectPlayer.setMuted(prefs.getBoolean(REFERENCE.PREF_MUTE, false));

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onWebsocketConnected() {
        notificationManager.notify(NOTIFICATION_ID, getServiceNotification("Connected"));
    }

    @Override
    public void onWebsocketDisconnected() {
        notificationManager.notify(NOTIFICATION_ID, getServiceNotification("Disconnected"));
    }

    @Override
    public ICancelableRequest requestConfig(Consumer<GameConfig> onSuccess) {
        GsonRequest<GameConfig> statusRequest = new GsonRequest<GameConfig>(getBaseUrl().append("get_config").toString(), s -> {
            s.getData().ifPresent(onSuccess);
        }, t -> {
            Log.e(TAG, "Failed to retrieve config", t);
            requestStatusUpdate = true;
        }).requireSuccess();
        requestQueue.add(statusRequest);
        return statusRequest;
    }

    @Override
    public ICancelableRequest requestScoreboard(Consumer<Scoreboard> onSuccess) {
        GsonRequest<Scoreboard> statusRequest = new GsonRequest<Scoreboard>(getBaseUrl().append("scoreboard").toString(), s -> {
            s.getData().ifPresent(onSuccess);
        }, t -> {
            Log.e(TAG, "Failed to retrieve scoreboard", t);
            requestStatusUpdate = true;
        }).requireSuccess();
        requestQueue.add(statusRequest);
        return statusRequest;
    }

    @Override
    public ICancelableRequest requestStatusUpdate(Consumer<GameStatus> onSuccess) {
        GsonRequest<GameStatus> statusRequest = new GsonRequest<GameStatus>(getBaseUrl().append("get_status").toString(), s -> {
            s.getData().ifPresent(onSuccess);
        }, t -> {
            Log.e(TAG, "Failed to retrieve status", t);
            requestStatusUpdate = true;
        }).requireSuccess();
        requestQueue.add(statusRequest);
        return statusRequest;
    }

    public ICancelableRequest requestVoiceserverInfo(Consumer<VoiceServerInfo> onSuccess) {
        GsonRequest<VoiceServerInfo> statusRequest = new GsonRequest<VoiceServerInfo>(getBaseUrl().append("voice_info").toString(), s -> {
            s.getData().ifPresent(onSuccess);
        }, t -> {
            Log.e(TAG, "Failed to retrieve voice server info", t);
        }).requireSuccess();
        requestQueue.add(statusRequest);
        return statusRequest;
    }

    @Override
    public boolean sendMessage(String msg) {
        return websocketHandler.send(msg);
    }

    @Override
    public void stop() {
        stopService(new Intent(this, ServerConnectionService.class));
    }

    @Override
    public AppConfig getAppConfig() {
        return appConfig;
    }

    private StringBuilder getBaseUrl() {
        return new StringBuilder(appConfig.isSecure() ? "https://" : "http://").append(appConfig.getServer()).append(':').append(appConfig.getPort()).append("/api/");
    }

    /**
     * Built the notification that should be displayed with the provided text
     */
    private Notification getServiceNotification(String text) {
        Notification.Builder notificationBuilder;
        if (Build.VERSION.SDK_INT >= 26) {
            notificationManager.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID, "serviceLaser", NotificationManager.IMPORTANCE_HIGH));
            notificationBuilder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID);
        } else {
            notificationBuilder = new Notification.Builder(this);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, GameActivity.class), 0);
        // Set the info for the views that show in the notification panel.
        return notificationBuilder
                .setTicker(text)  // the status text
                .setContentTitle(getText(R.string.server_connection_service))  // the label of the entry
                .setContentIntent(pendingIntent)
                .setContentText(text)  // the contents of the entry
                .setSmallIcon(R.drawable.tank)
                .build();

    }

    private void setupRequestQueue() {
        requestQueue = Volley.newRequestQueue(this); //TODO use a okhttpstack
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        String text = "Service started";

        // Send the notification.
        startForeground(NOTIFICATION_ID, getServiceNotification(text));
    }


}
