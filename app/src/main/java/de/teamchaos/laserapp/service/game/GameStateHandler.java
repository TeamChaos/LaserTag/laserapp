package de.teamchaos.laserapp.service.game;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import de.teamchaos.laserapp.data.*;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.network.AppWebsocketHandler;
import de.teamchaos.laserapp.service.sound.ISoundEffectPlayer;

import java.util.Objects;
import java.util.Optional;
import java.util.WeakHashMap;

/**
 * Provides the current game status to other app elements.
 * Processes game events and has a local representation of the current game state
 */
public class GameStateHandler implements AppWebsocketHandler.IAppWebsocketListener {
    private final static String TAG = "GameStateHandler";
    /**
     * Weak reference so even if e.g. a view fails to unregister it won't be kept in memory by this reference
     * Using a map here even though a list would be sufficient because {@link WeakHashMap} conveniently handles the weak references
     */
    private final WeakHashMap<IGameStateEventListener, GameStateHandler> listeners = new WeakHashMap<>();


    private final IServerConnectionService service;
    @Nullable
    private PlayerID playerID = null;

    @NonNull
    private GameStatus.State currentState = GameStatus.State.SETUP;
    /**
     * Null when {@link GameStateHandler#currentState} == Setup
     */
    @Nullable
    private GameConfig config;

    @Nullable
    private ScoreChartData scoreChartData;

    /**
     * Store the timestamp in millis we are currently waiting for (e.g. game end).
     * -1 if not active
     */
    private long nextTimeStamp;

    /**
     * If it is necessary to request a complete update once the websocket connection is established
     */
    private boolean requestStatusUpdate = true;

    /**
     * reference to a running status request so it can be canceled if necessary
     */
    @Nullable
    private IServerConnectionService.ICancelableRequest runningStatusRequest = null;
    /**
     * reference to a running scoreboard request so it can be canceled if necessary
     */
    @Nullable
    private IServerConnectionService.ICancelableRequest runningScoreboardRequest = null;
    /**
     * reference to a running config request so it can be canceled if necessary
     */
    @Nullable
    private IServerConnectionService.ICancelableRequest runningConfigRequest = null;
    private ISoundEffectPlayer soundEffectPlayer;

    public GameStateHandler(IServerConnectionService service) {
        this.service = service;
    }

    public void configure(ISoundEffectPlayer soundEffectPlayer, @Nullable PlayerID playerID) {
        this.playerID = playerID;
        this.soundEffectPlayer = soundEffectPlayer;
    }

    public GameStatus.State getCurrentState() {
        return currentState;
    }

    /**
     * Should not be empty after {@link GameStatus.State#SETUP}
     *
     * @return The game setup
     */
    public Optional<GameConfig> getGameSetup() {
        return Optional.ofNullable(config);
    }

    public Optional<Long> getNextTimestamp() {
        return this.nextTimeStamp == -1L ? Optional.empty() : Optional.of(nextTimeStamp);
    }

    /**
     * Should not be empty after {@link GameStatus.State#READY}
     *
     * @return The processed scorechart data
     */
    public Optional<ScoreChartData> getScoreChartData() {
        return Optional.ofNullable(scoreChartData);
    }

    @Override
    public void onMessage(WSResponse msg) {
        switch (msg.getEvent()) {
            case GAME_READY:
                this.updateConfig(msg.getConfig());
                this.updateState(GameStatus.State.READY, false);
                break;
            case CONFIG_CHANGED:
                this.updateConfig(config);
                break;
            case RESET:
                this.updateConfig(null);
                this.scoreChartData = null;
                this.updateState(GameStatus.State.SETUP, false);
                break;
            case KILL:
                this.handleKill(msg.getKillInfo());
                break;
            case SCOREBOARD:
                this.updateScoreboard(msg.getScoreboard());
                break;
            case GAME_STARTING:
                this.nextTimeStamp = this.getGameSetup().map(GameConfig::getSetup).filter(s -> s.getLaunchTrigger() == GameSetup.LAUNCH_TRIGGER.TIME).map(GameSetup::getLaunchTime).map(t -> System.currentTimeMillis() + t * 1000).orElse(-1L);
                this.updateState(GameStatus.State.STARTING, false);
                break;
            case GAME_STARTED:
                this.nextTimeStamp = this.getGameSetup().filter(s -> s.getStopTrigger() == GameConfig.STOP_TRIGGER.TIME).map(GameConfig::getTimeLimit).map(t -> System.currentTimeMillis() + t * 60 * 1000).orElse(-1L);
                this.updateState(GameStatus.State.RUNNING, false);
                break;
            case GAME_FINISHED:
                this.updateScoreboard(msg.getScoreboard());
                this.updateState(GameStatus.State.FINISHED, false);
                break;
            case GAME_STOPPED:
                this.updateState(GameStatus.State.EVALUATION, false);
                break;
        }
    }

    @Override
    public void onWebsocketConnected() {
        if (requestStatusUpdate) {
            requestStatusUpdate = false;
            if (runningStatusRequest != null) {
                runningStatusRequest.cancel();
                runningStatusRequest = null;
            }
            if (runningConfigRequest != null) {
                runningConfigRequest.cancel();
                runningConfigRequest = null;
            }
            if (runningScoreboardRequest != null) {
                runningScoreboardRequest.cancel();
                runningScoreboardRequest = null;
            }
            runningStatusRequest = service.requestStatusUpdate(status -> {
                this.runningStatusRequest = null;
                this.updateState(status.state, true);
                if (status.getTimer() != 0) {
                    this.nextTimeStamp = System.currentTimeMillis() + status.getTimer() * 1000;
                }
            });
        }
    }

    @Override
    public void onWebsocketDisconnected() {
        requestStatusUpdate = true;

    }

    public void registerListener(IGameStateEventListener listener) {
        listeners.put(listener, this);
    }

    public void unregisterListener(IGameStateEventListener listener) {
        listeners.remove(listener);
    }

    private void handleKill(KillInfo killInfo) {
        if (killInfo.getOffenderID().equals(playerID)) {
            boolean teamKill = getGameSetup().map(GameConfig::getSetup).map(GameSetup::getPlayers).map(p -> p.get(killInfo.getVictimID().getId()).getTeam() == p.get(killInfo.getOffenderID().getId()).getTeam()).orElse(false);
            soundEffectPlayer.playSound(teamKill ? ISoundEffectPlayer.SoundEvent.TEAMKILL : ISoundEffectPlayer.SoundEvent.KILL_CONFIRMED);
        }
    }

    private void notifyListeners(GameStatus.State newState) {
        listeners.keySet().forEach(l -> l.onStateChanged(newState));
    }

    private void notifyListeners(ScoreChartData data) {
        listeners.keySet().forEach(l -> l.onScoreboardUpdated(data));
    }

    /**
     * Update the local config object. If config changed, this resets the scoreboard data
     *
     * @param config
     */
    private void updateConfig(@Nullable GameConfig config) {
        if (runningConfigRequest != null) runningConfigRequest.cancel();
        if (!Objects.equals(this.config, config)) {
            Log.i(TAG, "Config changed");
            this.config = config;
            if (this.config != null) {
                this.scoreChartData = ScoreChartData.initFromSetup(this.config.getSetup());
            }
        }

    }

    /**
     * Update the scorechart data with the provided scoreboard
     *
     * @param scoreboard
     */
    private void updateScoreboard(Scoreboard scoreboard) {
        if (this.config == null) return;//Cannot process scoreboard without config
        if (runningScoreboardRequest != null) {
            runningScoreboardRequest.cancel();
            runningScoreboardRequest = null;
        }
        if (this.scoreChartData == null) {
            this.scoreChartData = ScoreChartData.initFromSetup(this.config.getSetup());
        }
        this.scoreChartData.processScoreboardUpdate(scoreboard);
        this.notifyListeners(scoreChartData);

    }

    /**
     * Make sure to call last, as this might trigger new requests
     *
     * @param forceRefreshData If scoreboard, config etc should be refreshed regardless of the previous state (e.g. on websocket reconnect).
     */
    private void updateState(GameStatus.State state, boolean forceRefreshData) {
        if (runningStatusRequest != null) {
            runningStatusRequest.cancel();
            runningStatusRequest = null;
        }
        if (state != this.currentState || forceRefreshData) {
            this.currentState = state;
            if (state.ordinal() > GameStatus.State.SETUP.ordinal()) {
                if (config == null || forceRefreshData) {
                    if (runningConfigRequest != null) runningConfigRequest.cancel();
                    runningConfigRequest = service.requestConfig(config -> {
                        runningConfigRequest = null;
                        this.updateConfig(config);
                        if (state.ordinal() > GameStatus.State.SETUP.ordinal() && (scoreChartData == null || forceRefreshData)) {
                            if (runningScoreboardRequest != null) runningScoreboardRequest.cancel();
                            runningScoreboardRequest = service.requestScoreboard(scoreboard -> {
                                runningScoreboardRequest = null;
                                this.updateScoreboard(scoreboard);
                            });
                        }
                    });
                } else if (state.ordinal() > GameStatus.State.SETUP.ordinal() && scoreChartData == null) {
                    if (runningScoreboardRequest != null) runningScoreboardRequest.cancel();
                    runningScoreboardRequest = service.requestScoreboard(scoreboard -> {
                        runningScoreboardRequest = null;
                        this.updateScoreboard(scoreboard);
                    });
                }
            } else {
                this.updateConfig(null);
            }
            notifyListeners(state);
            switch (state) {
                case STARTING:
                    soundEffectPlayer.playSound(ISoundEffectPlayer.SoundEvent.GET_READY);
                    break;
                case RUNNING:
                    soundEffectPlayer.playSound(ISoundEffectPlayer.SoundEvent.GO);
                    break;
                case EVALUATION:
                    soundEffectPlayer.playSound(ISoundEffectPlayer.SoundEvent.GAME_OVER);
                    break;
            }

        }

    }

    /**
     * Listens to game events
     */
    public interface IGameStateEventListener {
        /**
         * Handle changed scorechart data
         */
        default void onScoreboardUpdated(ScoreChartData updatedData) {
        }

        /**
         * Handle changed game state
         */
        default void onStateChanged(GameStatus.State newState) {
        }
    }

}
