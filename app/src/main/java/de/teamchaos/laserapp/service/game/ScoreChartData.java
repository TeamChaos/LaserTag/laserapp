package de.teamchaos.laserapp.service.game;


import android.util.Pair;
import de.teamchaos.laserapp.data.GameSetup;
import de.teamchaos.laserapp.data.Scoreboard;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Hold processed and sorted score data
 */
public class ScoreChartData {
    public static ScoreChartData initFromSetup(GameSetup setup) {
        ScoreChartData data = new ScoreChartData();

        data.gameMode = setup.getGameMode();
        data.teams = setup.getTeams();
        data.teamPlayers = new Int2ObjectArrayMap<>();
        data.teams.values().forEach(team -> data.teamPlayers.put(team.getID(), new ArrayList<>()));

        data.players = new Int2ObjectArrayMap<>();
        setup.getPlayers().values().forEach(p -> {
            Scoreboard.ScoreboardEntryData player = new Scoreboard.ScoreboardEntryData(p.getID(), p.getTeam(), p.getName());
            data.players.put(p.getID(), player);
            data.teamPlayers.getOrDefault(p.getTeam(), new ArrayList<>()).add(player);
        });

        return data;
    }

    /**
     * Map of player id to scoreboard entry.
     * Contains the actual scoreboard information
     */
    private Int2ObjectMap<Scoreboard.ScoreboardEntryData> players;
    /**
     * Maps of team id to team object
     */
    private Int2ObjectMap<GameSetup.Team> teams;
    /**
     * Map of team id to a list of scoreboard entries of all players of that team
     */
    private Int2ObjectMap<ArrayList<Scoreboard.ScoreboardEntryData>> teamPlayers;
    private GameSetup.GAME_MODE gameMode;

    public GameSetup.GAME_MODE getGameMode() {
        return gameMode;
    }

    /**
     * @return Unmodifiable map of player id to player scoreboard entry data
     */
    public Int2ObjectMap<Scoreboard.ScoreboardEntryData> getPlayers() {
        return Int2ObjectMaps.unmodifiable(players);
    }

    /**
     * @return Unmodifiable map of team id to list of player scoreboard entry data
     */
    public Int2ObjectMap<ArrayList<Scoreboard.ScoreboardEntryData>> getTeamPlayers() {
        return Int2ObjectMaps.unmodifiable(teamPlayers);
    }

    /**
     * @return Unmodifiable map of team id to team data
     */
    public Int2ObjectMap<GameSetup.Team> getTeams() {
        return teams;
    }

    public void processScoreboardUpdate(Scoreboard scoreboard) {
        for (Scoreboard.ScoreboardEntryData e : scoreboard.getScore()) {
            if (players.containsKey(e.getID())) {
                players.get(e.getID()).copyStatsFrom(e);
            }
        }
    }

    /**
     * Determine the leading player (based on score). If there are multiple leading players (or no players at all) this will return empty.
     *
     * @return Optional of direct reference to the leading player scoreboard data
     */
    public Optional<Scoreboard.ScoreboardEntryData> determineLeadingPlayer() {
        int minScore = Integer.MIN_VALUE;
        Scoreboard.ScoreboardEntryData leadingPlayer = null;
        for (Int2ObjectMap.Entry<Scoreboard.ScoreboardEntryData> entry : Int2ObjectMaps.fastIterable(players)) {
            int score = entry.getValue().getScore();
            if (score > minScore) {
                minScore = score;
                leadingPlayer = entry.getValue();
            } else if (score == minScore) {
                leadingPlayer = null; //If two players have equal score, there is no winner
            }
        }
        return Optional.ofNullable(leadingPlayer);

    }

    /**
     * Determine the leading team (based on score). If there are multiple leading teams (or no teams at all) this will return empty.
     *
     * @return Optional of pair of a) team b) score of that team
     */
    public Optional<Pair<GameSetup.Team, Integer>> determineLeadingTeam() {
        if (!getGameMode().isTeamBased) {
            return Optional.empty();
        }
        int maxScore = Integer.MIN_VALUE;
        GameSetup.Team leadingTeam = null;
        for (Int2ObjectMap.Entry<ArrayList<Scoreboard.ScoreboardEntryData>> entry : getTeamPlayers().int2ObjectEntrySet()) {
            int teamScore = entry.getValue().stream().mapToInt(Scoreboard.ScoreboardEntryData::getScore).sum();
            if (teamScore > maxScore) {
                maxScore = teamScore;
                leadingTeam = getTeams().get(entry.getIntKey());
            } else if (teamScore == maxScore) {
                leadingTeam = null;
            }
        }
        return leadingTeam != null ? Optional.of(Pair.create(leadingTeam, maxScore)) : Optional.empty();
    }

    /**
     * Determine the leading player/team depending on the gamemode (based on score). May be empty if there is a tie or no data.
     *
     * @return Pair of name and score
     */
    public Optional<Pair<String, Integer>> getLeading() {
        if (getGameMode().isTeamBased) {
            return determineLeadingTeam().map(p -> Pair.create(p.first.getName(), p.second));
        } else {
            return determineLeadingPlayer().map(p -> Pair.create(p.getName(), p.getScore()));
        }
    }


}
