package de.teamchaos.laserapp.service.map;

import android.location.Location;

import java.util.Optional;

/**
 * Handle locating the smartphone.
 * Manages storing last position and tracking and requesting updates
 */
public interface ILocationHandler {
    /**
     * @return Last known position. Empty if not determined yet
     */
    Optional<Location> getLastLocation();

    void registerListener(LocationHandler.ILocationListener listener);

    void unregisterListener(LocationHandler.ILocationListener listener);

    /**
     * Listen to location changes
     */
    interface ILocationListener {
        void onLocationChanged(Location location);
    }
}
