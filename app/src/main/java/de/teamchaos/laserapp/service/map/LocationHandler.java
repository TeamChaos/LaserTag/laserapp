package de.teamchaos.laserapp.service.map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import de.teamchaos.laserapp.data.GameStatus;
import de.teamchaos.laserapp.service.game.GameStateHandler;

import java.util.Optional;
import java.util.WeakHashMap;


public class LocationHandler implements LocationListener, GameStateHandler.IGameStateEventListener, ILocationHandler {

    private final static String TAG = "LocationHandler";
    private final Context context;
    /**
     * Weak reference so even if e.g. a view fails to unregister it won't be kept in memory by this reference
     * Using a map here even though a list would be sufficient because {@link WeakHashMap} conveniently handles the weak references
     */
    private final WeakHashMap<ILocationListener, LocationHandler> listeners = new WeakHashMap<>();
    @Nullable
    private Location lastLocation;

    public LocationHandler(Context context) {
        this.context = context;
    }

    @Override
    public Optional<Location> getLastLocation() {
        return Optional.ofNullable(lastLocation);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "New location " + location.getLatitude() + ":" + location.getLongitude());
        listeners.keySet().forEach(l -> l.onLocationChanged(location));

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i(TAG, "Disabled prover " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStateChanged(GameStatus.State newState) {
        startTrackingLocation(newState == GameStatus.State.RUNNING);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void registerListener(ILocationListener listener) {
        listeners.put(listener, this);
    }

    public void startTrackingLocation(boolean highFrequency) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Start tracking location");
            locationManager.removeUpdates(this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, highFrequency ? 1000 : 60 * 1000, 1, this);
        } else {
            Log.i(TAG, "Could not start location tracking");
        }
    }

    @Override
    public void unregisterListener(ILocationListener listener) {
        listeners.remove(listener);
    }

}
