package de.teamchaos.laserapp.service.network;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import androidx.annotation.Nullable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import de.teamchaos.laserapp.data.AdapterFactory;
import de.teamchaos.laserapp.data.AppConfig;
import de.teamchaos.laserapp.data.PlayerID;
import de.teamchaos.laserapp.data.WSResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocketListener;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the communication with the websocket server
 */
public class AppWebsocketHandler implements IAppWebsocketHandler {
    private static final Gson gson = new GsonBuilder().registerTypeAdapterFactory(new AdapterFactory()).create();
    private final static String TAG = "AppWebsocketHandler";
    private final OkHttpClient webSocketClient = new OkHttpClient();
    private final Listener webSocketListener = new Listener();
    private final List<IAppWebsocketListener> listenerList = new ArrayList<>();
    private final Handler handler;
    private String serverUrl;
    /**
     * If the websocket process has been started. Does not guarantee that a connection is established
     */
    private boolean wsActive;
    private int playerID = -1;
    private String credentials = null;

    public AppWebsocketHandler(Context context) {
        handler = new Handler(context.getMainLooper());
    }

    public void checkWebsocket() {
        if (!wsActive) {
            wsActive = true;
            if (playerID == -1 || credentials == null || serverUrl == null)
                throw new IllegalStateException("Must setup before checking websocket");
            Request request = new Request.Builder().url(serverUrl).addHeader("Authorization", "Basic " + credentials).build();
            webSocketClient.newWebSocket(request, webSocketListener);
        }
    }

    @Override
    public boolean isWebsocketConnected() {
        return wsActive && webSocketListener.mWebSocket != null;
    }

    @Override
    public void registerListener(IAppWebsocketListener listener) {
        this.listenerList.add(listener);
    }

    @Override
    public boolean send(String msg) {
        return isWebsocketConnected() && webSocketListener.send(msg);
    }

    /**
     * Update authorization parameters. Call {@link AppWebsocketHandler#checkWebsocket()} (regularly) afterwards, as this initiates a websocket disconnect if params have changed
     *
     * @param config AppConfig
     */
    public void setup(@Nonnull AppConfig config) {
        serverUrl = (config.isSecure() ? "wss://" : "ws://") + config.getServer() + ":" + config.getPort() + "/ws_app";
        if (this.playerID != config.getPlayerid().map(PlayerID::getId).orElse(-1) || !config.getCredentials().equals(this.credentials)) {
            this.playerID = config.getPlayerid().map(PlayerID::getId).orElse(-1);
            this.credentials = config.getCredentials();
            if (wsActive) {
                stop("Reauth");
            }
        }
    }

    public void stop(String reason) {
        webSocketListener.closeSocket(reason);
    }

    @Override
    public void unregisterListener(IAppWebsocketListener listener) {
        this.listenerList.remove(listener);
    }

    private void onMessage(String msg) {
        try {
            WSResponse response = gson.fromJson(msg, WSResponse.class);
            handler.post(() -> listenerList.forEach(listener -> listener.onMessage(response)));
        } catch (JsonParseException e) {
            Log.e(TAG, "Failed to process message: " + msg, e);
        }

    }

    private void onWebsocketConnected() {
        handler.post(() -> listenerList.forEach(IAppWebsocketListener::onWebsocketConnected));
    }

    private void onWebsocketDisconnected() {
        handler.post(() -> listenerList.forEach(IAppWebsocketListener::onWebsocketDisconnected));
    }


    /**
     * Listens to the raw okhttp websockets events
     */
    private final class Listener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;
        private okhttp3.WebSocket mWebSocket;

        public void closeSocket(String reason) {
            mWebSocket.close(NORMAL_CLOSURE_STATUS, reason);
        }

        @Override
        public void onClosed(okhttp3.WebSocket webSocket, int code, String reason) {
            Log.w(TAG, "WS closed " + code + reason);
            wsActive = false;
            mWebSocket = null;

            onWebsocketDisconnected();
        }

        @Override
        public void onClosing(okhttp3.WebSocket webSocket, int code, String reason) {
            Log.w(TAG, "WS closing " + code + reason);
            mWebSocket = null;
            wsActive = false;
        }

        @Override
        public void onFailure(okhttp3.WebSocket webSocket, Throwable t, @Nullable Response response) {
            Log.e(TAG, "WS failed", t);
            if (mWebSocket != null) {//Only notify listener if connection was successful before
                onWebsocketDisconnected();
                mWebSocket = null;
            }
            wsActive = false;
        }

        @Override
        public void onMessage(okhttp3.WebSocket webSocket, String text) {
            Log.i(TAG, "WSMessage " + text);
            AppWebsocketHandler.this.onMessage(text);
        }

        @Override
        public void onOpen(okhttp3.WebSocket webSocket, Response response) {
            Log.i(TAG, "WSOpen");
            mWebSocket = webSocket;
            onWebsocketConnected();
        }

        /**
         * Does not check if socket is active
         *
         * @param msg raw String message
         * @return Whether the message was successfully enqueued
         */
        public boolean send(String msg) {
            return mWebSocket.send(msg);
        }
    }
}
