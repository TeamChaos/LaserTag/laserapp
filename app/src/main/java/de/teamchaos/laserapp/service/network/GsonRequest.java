package de.teamchaos.laserapp.service.network;

import androidx.annotation.Nullable;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import de.teamchaos.laserapp.data.APIResponse;
import de.teamchaos.laserapp.data.AdapterFactory;
import de.teamchaos.laserapp.service.IServerConnectionService;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Volley request that processes the response as {@link APIResponse} using GSON
 *
 * @param <T>
 */
public class GsonRequest<T> extends Request<APIResponse<T>> implements IServerConnectionService.ICancelableRequest {
    private static final Gson gson = new GsonBuilder().registerTypeAdapterFactory(new AdapterFactory()).create();
    private final Map<String, String> headers;
    private final Response.Listener<APIResponse<T>> listener;
    private boolean requireSucess = false;


    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url URL of the request to make
     */
    public GsonRequest(String url,
                       Response.Listener<APIResponse<T>> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.headers = new HashMap<>();
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    public GsonRequest<T> requireSuccess() {
        this.requireSucess = true;
        return this;
    }

    @Override
    protected void deliverResponse(APIResponse<T> response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<APIResponse<T>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            APIResponse<T> parsedResponse = gson.fromJson(json, APIResponse.class);
            if (requireSucess && !parsedResponse.isSuccess()) {
                return Response.error(new RequestFailed(parsedResponse.getStatus()));
            }
            return Response.success(parsedResponse, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    /**
     * Error used when the http request itself was fine, but the server provided an "error" code
     */
    public static class RequestFailed extends VolleyError {
        private final APIResponse.STATUS status;


        public RequestFailed(APIResponse.STATUS status) {
            this.status = status;
        }

        @Nullable
        @Override
        public String getMessage() {
            return "Server responded with: " + status.name();
        }
    }

}
