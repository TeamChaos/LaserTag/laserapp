package de.teamchaos.laserapp.service.network;

import de.teamchaos.laserapp.data.WSResponse;

/**
 * Handles the communication with the websocket server
 */
public interface IAppWebsocketHandler {

    boolean isWebsocketConnected();

    /**
     * Listen to events. Don't forget to unregister listener.
     */
    void registerListener(AppWebsocketHandler.IAppWebsocketListener listener);

    /**
     * Send message to the ws server
     *
     * @param msg raw String message
     * @return Whether the message was successfully enqueued
     */
    boolean send(String msg);

    void unregisterListener(AppWebsocketHandler.IAppWebsocketListener listener);

    /**
     * Listens to websocket events
     */
    interface IAppWebsocketListener {
        /**
         * Handle parsed websocket message
         */
        default void onMessage(WSResponse msg) {

        }

        /**
         * Handle websocket connection established (again)
         */
        default void onWebsocketConnected() {

        }

        /**
         * Handle websocket connection lost
         */
        default void onWebsocketDisconnected() {

        }
    }
}
