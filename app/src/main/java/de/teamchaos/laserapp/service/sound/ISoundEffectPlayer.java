package de.teamchaos.laserapp.service.sound;

/**
 * Manages playback of all sound effects
 */
public interface ISoundEffectPlayer {
    /**
     * Cancel playback of a previously started endlessly looped sound.
     *
     * @param streamID Received stream id
     */
    void cancelEndlessSound(int streamID);

    void playSound(SoundEffectPlayer.SoundEvent sound);

    /**
     * Start a endlessly looped sound
     *
     * @return stream id required to cancel playback
     */
    int startEndlessSound(ISoundEffectPlayer.SoundEvent sound);

    enum SoundEvent {
        KILL_CONFIRMED, GET_READY, GO, GAME_OVER, TEAMKILL, RADIO_NOISE
    }
}
