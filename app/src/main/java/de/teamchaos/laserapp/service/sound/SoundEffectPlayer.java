package de.teamchaos.laserapp.service.sound;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import de.teamchaos.laserapp.R;
import it.unimi.dsi.fastutil.ints.Int2IntArrayMap;


public class SoundEffectPlayer implements ISoundEffectPlayer {

    private final Int2IntArrayMap soundMap = new Int2IntArrayMap();
    private SoundPool soundPool;
    private boolean muted = false;


    @Override
    public void cancelEndlessSound(int streamID) {
        soundPool.stop(streamID);
    }

    public void loadSounds(Context context) {
        soundPool = new SoundPool.Builder().setMaxStreams(1).setAudioAttributes(new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SPEECH).setUsage(AudioAttributes.USAGE_GAME).build()).build();
        soundMap.put(ISoundEffectPlayer.SoundEvent.KILL_CONFIRMED.ordinal(), soundPool.load(context, R.raw.kill_confirmed, 1));
        soundMap.put(SoundEvent.GET_READY.ordinal(), soundPool.load(context, R.raw.get_ready, 1));
        soundMap.put(SoundEvent.GO.ordinal(), soundPool.load(context, R.raw.go, 1));
        soundMap.put(SoundEvent.GAME_OVER.ordinal(), soundPool.load(context, R.raw.game_over, 1));
        soundMap.put(SoundEvent.TEAMKILL.ordinal(), soundPool.load(context, R.raw.teamkill, 1));
        soundMap.put(SoundEvent.RADIO_NOISE.ordinal(), soundPool.load(context, R.raw.radio_noise, 1));
    }

    @Override
    public void playSound(SoundEvent sound) {
        if (!muted) soundPool.play(soundMap.get(sound.ordinal()), 1, 1, 3, 0, 1);
    }

    public void setMuted(boolean checked) {
        this.muted = checked;
    }

    @Override
    public int startEndlessSound(SoundEvent sound) {
        return soundPool.play(soundMap.get(sound.ordinal()), 1, 1, 0, -1, 1);
    }
}
