package de.teamchaos.laserapp.service.voice;

import android.util.Log;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.ServerConnectionService;
import de.teamchaos.laserapp.service.sound.ISoundEffectPlayer;
import se.lublin.humla.audio.inputmode.ToggleInputMode;


/**
 * Modified version of Humlas "Push-To-Talk" input mode.
 * Triggers playback of some "radio noise" while the input is enabled
 */
public class CustomToggleInputMode extends ToggleInputMode {
    private int activeStream = -1;

    @Override
    public void setTalkingOn(boolean talking) {
        super.setTalkingOn(talking);
        Log.i("CustomToggleInputMode", "Toggling input enabled: " + talking);
        if (talking) {
            activeStream = ServerConnectionService.getInstance().map(IServerConnectionService::getSoundEffectPlayer).map(player -> player.startEndlessSound(ISoundEffectPlayer.SoundEvent.RADIO_NOISE)).orElse(-1);

        } else if (activeStream != -1) {
            ServerConnectionService.getInstance().map(IServerConnectionService::getSoundEffectPlayer).ifPresent(player -> player.cancelEndlessSound(activeStream));
            activeStream = -1;

        }
    }
}
