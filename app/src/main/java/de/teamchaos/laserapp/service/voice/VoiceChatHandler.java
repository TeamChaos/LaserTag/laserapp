package de.teamchaos.laserapp.service.voice;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.KeyEvent;
import androidx.core.app.ActivityCompat;
import de.teamchaos.laserapp.BuildConfig;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.data.VoiceServerInfo;
import org.minidns.dnsserverlookup.android21.AndroidUsingLinkProperties;
import se.lublin.humla.Constants;
import se.lublin.humla.HumlaService;
import se.lublin.humla.audio.AudioOutput;
import se.lublin.humla.audio.BluetoothScoReceiver;
import se.lublin.humla.audio.inputmode.ActivityInputMode;
import se.lublin.humla.audio.inputmode.ToggleInputMode;
import se.lublin.humla.audio.javacpp.CELT7;
import se.lublin.humla.exception.AudioException;
import se.lublin.humla.exception.NotSynchronizedException;
import se.lublin.humla.model.Server;
import se.lublin.humla.model.TalkState;
import se.lublin.humla.model.User;
import se.lublin.humla.model.WhisperTargetList;
import se.lublin.humla.net.HumlaConnection;
import se.lublin.humla.net.HumlaTCPMessageType;
import se.lublin.humla.protobuf.Mumble;
import se.lublin.humla.protocol.AudioHandler;
import se.lublin.humla.protocol.ModelHandler;
import se.lublin.humla.util.HumlaCallbacks;
import se.lublin.humla.util.HumlaException;
import se.lublin.humla.util.HumlaLogger;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Handle voice chat with Mumble/Murmur voice server based on the humla library
 */
public class VoiceChatHandler implements HumlaConnection.HumlaConnectionListener, BluetoothScoReceiver.Listener, HumlaLogger {
    private final static String TAG = "VoiceChat";

    // Service settings
    private final Context context;
    private final String mCertificatePassword = "";
    private final boolean mForceTcp = false;
    private final List<String> mAccessTokens = new ArrayList<>();
    private final List<Integer> mLocalMuteHistory = new ArrayList<>();
    private final List<Integer> mLocalIgnoreHistory = new ArrayList<>();
    private final AudioHandler.Builder mAudioBuilder;
    private final WhisperTargetList mWhisperTargetList;
    private final Handler mHandler;
    private final HumlaCallbacks mCallbacks;
    private final BluetoothScoReceiver mBluetoothReceiver;
    private final ActivityInputMode mActivityInputMode;
    private final ToggleInputMode mToggleInputMode;
    private final MediaSessionCompat mediaSession;
    private final PlaybackStateCompat.Builder playbackStateBuilder;
    private Server mServer;
    private boolean mAutoReconnect;
    private int mAutoReconnectDelay;
    private byte[] mCertificate;
    private boolean mUseOpus;
    private String mClientName;
    private int mTrustStore;
    private String mTrustStorePassword;
    private String mTrustStoreFormat;
    private byte mVoiceTargetId;
    private HumlaConnection mConnection;
    private HumlaService.ConnectionState mConnectionState;
    private ModelHandler mModelHandler;
    private final AudioHandler.AudioEncodeListener mAudioInputListener =
            new AudioHandler.AudioEncodeListener() {
                @Override
                public void onAudioEncoded(byte[] data, int length) {
                    if (mConnection != null && mConnection.isSynchronized()) {
                        mConnection.sendUDPMessage(data, length, false);
                    }
                }

                @Override
                public void onTalkingStateChanged(final boolean talking) {
                    mHandler.post(() -> {
                        try {
                            // If the server session is inactive, ignore this message.
                            // It's likely that this is leftover from a terminated connection.
                            if (!isSynchronized())
                                return;

                            if (mModelHandler == null || mConnection == null) {
                                return;
                            }
                            final User currentUser = mModelHandler.getUser(mConnection.getSession());
                            if (currentUser == null) return;

                            currentUser.setTalkState(talking ? TalkState.TALKING : TalkState.PASSIVE);
                            mCallbacks.onUserTalkStateUpdated(currentUser);
                        } catch (NotSynchronizedException e) {
                            e.printStackTrace();
                        }
                    });
                }
            };
    private final AudioOutput.AudioOutputListener mAudioOutputListener = new AudioOutput.AudioOutputListener() {
        @Override
        public User getUser(int session) {
            if (mModelHandler != null) {
                return mModelHandler.getUser(session);
            }
            return null;
        }

        @Override
        public void onUserTalkStateUpdated(final User user) {
            mCallbacks.onUserTalkStateUpdated(user);
        }
    };
    private AudioHandler mAudioHandler;
    private boolean mReconnecting;
    /**
     * Listen for connectivity changes in the reconnection state, and reconnect accordingly.
     */
    private final BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!mReconnecting) {
                VoiceChatHandler.this.context.unregisterReceiver(this);
                return;
            }

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                Log.v(Constants.TAG, "Connectivity restored, attempting reconnect.");
                connect();
            }
        }
    };

    public VoiceChatHandler(Context context) {
        this.context = context;
        mHandler = new Handler(context.getMainLooper());
        mCallbacks = new HumlaCallbacks();
        mAudioBuilder = new AudioHandler.Builder()
                .setContext(context)
                .setLogger(this)
                .setEncodeListener(mAudioInputListener)
                .setTalkingListener(mAudioOutputListener);
        mConnectionState = HumlaService.ConnectionState.DISCONNECTED;
        mBluetoothReceiver = new BluetoothScoReceiver(context, this);
        context.registerReceiver(mBluetoothReceiver, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED));
        mToggleInputMode = new CustomToggleInputMode();
        mActivityInputMode = new ActivityInputMode(0);
        mWhisperTargetList = new WhisperTargetList();

        // initialize minidns dns lookup mechanisms
        AndroidUsingLinkProperties.setup(context);

        mediaSession = new MediaSessionCompat(context, TAG);
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS);
        MediaMetadataCompat.Builder builder = new MediaMetadataCompat.Builder();
        builder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, "Voice Chat");
        mediaSession.setMetadata(builder.build());
        playbackStateBuilder = new PlaybackStateCompat.Builder().setState(PlaybackStateCompat.STATE_NONE, 0, 0);
        mediaSession.setCallback(new MediaSessionCompat.Callback() {
            @Override
            public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
                KeyEvent keyEvent = mediaButtonEvent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                if (keyEvent == null) {
                    return false;
                }
                int keyCode = keyEvent.getKeyCode();
                if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                    //Log.i(TAG, "Got play pause " + (keyEvent.getAction() == KeyEvent.ACTION_UP ? "up" : "down"));
                    if (KeyEvent.ACTION_DOWN == keyEvent.getAction()) mToggleInputMode.toggleTalkingOn();
                }
                return true;
            }
        });

    }

    public void changeInputMode(boolean pushToTalk) {
        mAudioBuilder.setInputMode(pushToTalk ? mToggleInputMode : mActivityInputMode);
        if (!pushToTalk && mToggleInputMode.isTalkingOn())
            mToggleInputMode.setTalkingOn(false); //Disable pushToTalk state and static noise in case it was previously enabled
        if (mAudioHandler != null && mAudioHandler.isInitialized()) {
            try {
                createAudioHandler();
                Log.i(TAG, "Audio subsystem reloaded after settings change.");
            } catch (AudioException e) {
                Log.e(TAG, "Failed to reload audio system after settings change", e);
            }

        }
    }

    public void disconnect() {
        if (mConnection != null) {
            mConnection.disconnect();
        }
    }

    /**
     * Exposes the current connection. The current connection is set once an attempt to connect to
     * a server is made, and remains set until a subsequent connection. It remains available
     * after disconnection to provide information regarding the terminated connection.
     *
     * @return The active {@link HumlaConnection}.
     */
    public HumlaConnection getConnection() {
        return mConnection;
    }

    public boolean isConnectionEstablished() {
        return mConnection != null && mConnection.isConnected();
    }

    /**
     * @return true if Humla has received the ServerSync message, indicating synchronization with
     * the server's model and settings. This is the main state of the service.
     */
    public boolean isSynchronized() {
        return mConnection != null && mConnection.isSynchronized();
    }

    @Override
    public void logError(String message) {
        mCallbacks.onLogError(message);
        Log.e(TAG, message);
    }

    @Override
    public void logInfo(String message) {
        if (mConnection == null || !mConnection.isSynchronized())
            return; // don't log info prior to synchronization
        mCallbacks.onLogInfo(message);
        Log.i(TAG, message);
    }

    @Override
    public void logWarning(String message) {
        mCallbacks.onLogWarning(message);
        Log.w(TAG, message);
    }

    @Override
    public void onBluetoothScoConnected() {
        // After an SCO connection is established, audio is rerouted to be compatible with SCO.
        mAudioBuilder.setBluetoothEnabled(true);
        if (mAudioHandler != null) {
            try {
                createAudioHandler();
            } catch (AudioException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBluetoothScoDisconnected() {
        // Restore audio settings after disconnection.
        mAudioBuilder.setBluetoothEnabled(false);
        if (mAudioHandler != null) {
            try {
                createAudioHandler();
            } catch (AudioException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onConnectionDisconnected(HumlaException e) {
        if (e != null) {
            Log.e(Constants.TAG, "Error: " + e.getMessage() +
                    " (reason: " + e.getReason().name() + ")");
            mConnectionState = HumlaService.ConnectionState.CONNECTION_LOST;

            setReconnecting(mAutoReconnect
                    && e.getReason() == HumlaException.HumlaDisconnectReason.CONNECTION_ERROR);
        } else {
            Log.v(Constants.TAG, "Disconnected");
            mConnectionState = HumlaService.ConnectionState.DISCONNECTED;
        }


        if (mAudioHandler != null) {
            mAudioHandler.shutdown();
        }

        mModelHandler = null;
        mAudioHandler = null;
        mVoiceTargetId = 0;
        mWhisperTargetList.clear();

        // Halt SCO connection on shutdown.
        mBluetoothReceiver.stopBluetoothSco();

        mCallbacks.onDisconnected(e);
        disableMediaSession();
    }

    @Override
    public void onConnectionEstablished() {
        // Send version information and authenticate.
        final Mumble.Version.Builder version = Mumble.Version.newBuilder();
        version.setRelease(mClientName);
        version.setVersion(Constants.PROTOCOL_VERSION);
        version.setOs("Android");
        version.setOsVersion(Build.VERSION.RELEASE);

        final Mumble.Authenticate.Builder auth = Mumble.Authenticate.newBuilder();
        auth.setUsername(mServer.getUsername());
        auth.setPassword(mServer.getPassword());
        auth.addCeltVersions(CELT7.getBitstreamVersion());
        // FIXME: resolve issues with CELT 11 robot voices.
//            auth.addCeltVersions(Constants.CELT_11_VERSION);
        auth.setOpus(mUseOpus);
        auth.addAllTokens(mAccessTokens);

        mConnection.sendTCPMessage(version.build(), HumlaTCPMessageType.Version);
        mConnection.sendTCPMessage(auth.build(), HumlaTCPMessageType.Authenticate);
    }

    @Override
    public void onConnectionHandshakeFailed(X509Certificate[] chain) {
        mCallbacks.onTLSHandshakeFailed(chain);
    }

    @Override
    public void onConnectionSynchronized() {
        // early disconned?
        if (!mConnection.isConnected()) {
            return;
        }

        // TODO hackish, but this seems to happen?!
        if (mModelHandler == null) {
            Log.e(Constants.TAG, "Error in HumlaService.onConnectionSynchronized: mAudioHandler is null");
            return;
        }

        mConnectionState = HumlaService.ConnectionState.CONNECTED;

        Log.v(Constants.TAG, "Connected");

        try {
            mAudioHandler = mAudioBuilder.initialize(
                    mModelHandler.getUser(mConnection.getSession()),
                    mConnection.getMaxBandwidth(), mConnection.getCodec(),
                    mVoiceTargetId);
            mConnection.addTCPMessageHandlers(mAudioHandler);
            mConnection.addUDPMessageHandlers(mAudioHandler);
        } catch (AudioException e) {
            e.printStackTrace();
            onConnectionWarning(e.getMessage());
        } catch (NotSynchronizedException e) {
            throw new RuntimeException("Connection should be synchronized in callback for synchronization!", e);
        }

        mCallbacks.onConnected();
        enableMediaSession();
    }

    @Override
    public void onConnectionWarning(String warning) {
        logWarning(warning);
    }

    public void setReconnecting(boolean reconnecting) {
        if (mReconnecting == reconnecting)
            return;

        mReconnecting = reconnecting;
        if (reconnecting) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                Log.v(Constants.TAG, "Connection lost due to non-connectivity issue. Start reconnect polling.");
                Handler mainHandler = new Handler();
                mainHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mReconnecting) connect();
                    }
                }, mAutoReconnectDelay);
            } else {
                // In the event that we've lost connectivity, don't poll. Wait until network
                // returns before we resume connection attempts.
                Log.v(Constants.TAG, "Connection lost due to connectivity issue. Waiting until network returns.");
                try {
                    context.registerReceiver(mConnectivityReceiver,
                            new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try {
                context.unregisterReceiver(mConnectivityReceiver);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

    public void start(VoiceServerInfo info, int playerID, boolean pushToTalk) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
            return;
        Log.i(TAG, "Connecting to " + info.getHost() + " " + info.getPort());
        String userName = "AppPlayer" + playerID;
        mServer = new Server(0, "GameVoiceServer", info.getHost(), info.getPort(), userName, info.getPassword());
        mAutoReconnect = true;
        mAutoReconnectDelay = 3000;
        mActivityInputMode.setThreshold(0.6f);
        mAudioBuilder.setAmplitudeBoost(1);
        mAudioBuilder.setInputMode(pushToTalk ? mToggleInputMode : mActivityInputMode);
        mAudioBuilder.setInputSampleRate(48000);
        mAudioBuilder.setTargetBitrate(40000);
        mAudioBuilder.setTargetFramesPerPacket(2);
        mUseOpus = true;
        mClientName = BuildConfig.APPLICATION_ID;
        int audioSource = MediaRecorder.AudioSource.MIC;
        int audioStream = AudioManager.STREAM_VOICE_CALL;
        mAudioBuilder.setAudioSource(audioSource);
        mAudioBuilder.setAudioStream(audioStream);
        mTrustStore = R.raw.mumble_keystore;
        mTrustStorePassword = "test123";
        mTrustStoreFormat = "BKS";
        mAudioBuilder.setPreprocessorEnabled(true);
        connect();
    }

    public void stop() {
        disconnect();
        context.unregisterReceiver(mBluetoothReceiver);
        mediaSession.release();
    }

    protected void connect() {
        try {
            setReconnecting(false);
            mConnectionState = HumlaService.ConnectionState.DISCONNECTED;
            mVoiceTargetId = 0;
            mWhisperTargetList.clear();

            mConnection = new HumlaConnection(this);
            mConnection.setForceTCP(mForceTcp);
            mConnection.setUseTor(false);
            mConnection.setKeys(mCertificate, mCertificatePassword);

            try {
                final KeyStore ks = KeyStore.getInstance(mTrustStoreFormat);
                final InputStream in = this.context.getResources().openRawResource(mTrustStore);
                try {
                    // don't forget to put the password used above in strings.xml/mystore_password
                    ks.load(in, mTrustStorePassword.toCharArray());
                } finally {
                    in.close();
                }
                mConnection.setTrustStore(ks);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            mModelHandler = new ModelHandler(context, mCallbacks, this,
                    mLocalMuteHistory, mLocalIgnoreHistory);
            mConnection.addTCPMessageHandlers(mModelHandler);

            mConnectionState = HumlaService.ConnectionState.CONNECTING;

            mCallbacks.onConnecting();
            mConnection.connect(mServer.getSrvHost(), mServer.getSrvPort());
        } catch (HumlaException e) {
            e.printStackTrace();
            mCallbacks.onDisconnected(e);
        }
    }

    /**
     * Instantiates an audio handler with the current service settings, destroying any previous
     * handler. Requires synchronization with the server, as the maximum bandwidth and session must
     * be known.
     */
    private void createAudioHandler() throws AudioException {
        if (BuildConfig.DEBUG && mConnectionState != HumlaService.ConnectionState.CONNECTED) {
            throw new AssertionError("Attempted to instantiate audio handler when not connected!");
        }

        if (mAudioHandler != null) {
            mConnection.removeTCPMessageHandler(mAudioHandler);
            mConnection.removeUDPMessageHandler(mAudioHandler);
            mAudioHandler.shutdown();
        }

        try {
            mAudioHandler = mAudioBuilder.initialize(
                    mModelHandler.getUser(mConnection.getSession()),
                    mConnection.getMaxBandwidth(), mConnection.getCodec(),
                    mVoiceTargetId);
            mConnection.addTCPMessageHandlers(mAudioHandler);
            mConnection.addUDPMessageHandlers(mAudioHandler);
        } catch (NotSynchronizedException e) {
            throw new RuntimeException("Attempted to create audio handler when not synchronized!");
        }
    }

    private void disableMediaSession() {
        mediaSession.setActive(false);
        mediaSession.setPlaybackState(playbackStateBuilder.setState(PlaybackStateCompat.STATE_NONE, 0, 1).setActions(0).build());

    }

    private void enableMediaSession() {
        mediaSession.setActive(true);
        mediaSession.setPlaybackState(playbackStateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, 0, 1).setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PAUSE).build());
    }

    /**
     * Returnes the current {@link AudioHandler}. An AudioHandler is instantiated upon connection
     * to a server, and destroyed upon disconnection.
     *
     * @return the active AudioHandler, or null if there is no active connection.
     */
    private AudioHandler getAudioHandler() throws NotSynchronizedException {
        if (!isSynchronized())
            throw new NotSynchronizedException();
        if (mAudioHandler == null && mConnectionState == HumlaService.ConnectionState.CONNECTED)
            throw new RuntimeException("Audio handler should always be instantiated while connected!");
        return mAudioHandler;
    }

    /**
     * Returns the bluetooth service provider, established after synchronization.
     *
     * @return The {@link BluetoothScoReceiver} attached to this service.
     */
    private BluetoothScoReceiver getBluetoothReceiver() throws NotSynchronizedException {
        if (!isSynchronized())
            throw new NotSynchronizedException();
        return mBluetoothReceiver;
    }

    /**
     * Returns the current {@link ModelHandler}, containing the channel tree. A model handler is
     * valid for the lifetime of a connection.
     *
     * @return the active ModelHandler, or null if there is no active connection.
     */
    private ModelHandler getModelHandler() throws NotSynchronizedException {
        if (!isSynchronized())
            throw new NotSynchronizedException();
        if (mModelHandler == null && mConnectionState == HumlaService.ConnectionState.CONNECTED)
            throw new RuntimeException("Model handler should always be instantiated while connected!");
        return mModelHandler;
    }


}
