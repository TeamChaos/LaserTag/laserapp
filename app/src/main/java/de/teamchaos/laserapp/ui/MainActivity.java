package de.teamchaos.laserapp.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;
import de.teamchaos.laserapp.BuildConfig;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.data.AdapterFactory;
import de.teamchaos.laserapp.data.AppConfig;
import de.teamchaos.laserapp.data.PlayerID;
import de.teamchaos.laserapp.service.ServerConnectionService;
import de.teamchaos.laserapp.ui.game.GameActivity;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main";
    final int RequestCameraPermissionID = 1001;
    private final Gson configGson = new GsonBuilder().registerTypeAdapterFactory(new AdapterFactory()).create();
    private DecoratedBarcodeView barcodeView;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    barcodeView.resume();
                }
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ServerConnectionService.getInstance().isPresent()) {
            startActivity(new Intent(this, GameActivity.class));
        }
        setContentView(R.layout.activity_main);

        @Nullable final AppConfig oldConfig = getIntent().getParcelableExtra(ServerConnectionService.START_INTENT_CONFIG);

        LinearLayout player_id_btn_layout = findViewById(R.id.player_id_btn_layout);
        TextView firstLine = findViewById(R.id.tv_select_player);
        if (BuildConfig.DEBUG) {
            firstLine.setText("Select player");
            int maxPlayers = 4;
            for (int i = 0; i < maxPlayers; i++) {
                Button player_id_btn = new Button(this);
                player_id_btn.setText(String.valueOf(i));
                player_id_btn.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                String credentials = new String(Base64.encode((i + ":" + "dummy").getBytes(), Base64.NO_PADDING | Base64.NO_WRAP));
                AppConfig appConfigDebug = new AppConfig(BuildConfig.DEBUG_SERVER_URL, BuildConfig.DEBUG_SERVER_PORT, BuildConfig.DEBUG_SERVER_SECURE, credentials, new PlayerID(i));
                player_id_btn.setOnClickListener(v -> startServiceAndShowNetworkSettings(appConfigDebug));
                player_id_btn_layout.addView(player_id_btn);

            }
        } else if (oldConfig != null) {
            Button load_prev = new Button(this);
            load_prev.setText(R.string.btn_load_previous);
            load_prev.setOnClickListener(v -> {
                startServiceAndShowNetworkSettings(oldConfig);
            });
            player_id_btn_layout.addView(load_prev);
        } else {
            player_id_btn_layout.setVisibility(View.GONE);
        }


        barcodeView = findViewById(R.id.barcode_scanner);
        barcodeView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(Collections.singletonList(BarcodeFormat.QR_CODE)));
        barcodeView.decodeSingle(result -> {
            String qrData = result.getText();
            try {
                AppConfig config = configGson.fromJson(qrData, AppConfig.class);
                Log.i(TAG, "Found configuration: " + config);
                startServiceAndShowNetworkSettings(config);
            } catch (JsonSyntaxException e) {
                Log.w(TAG, "Failed to parse qr code json: ", e);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        barcodeView.pauseAndWait();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //Request permission
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA}, RequestCameraPermissionID);
        } else {
            barcodeView.resume();

        }
    }

    private void startServiceAndShowNetworkSettings(AppConfig config) {
        Intent serviceIntent = new Intent(this, ServerConnectionService.class);
        serviceIntent.putExtra(ServerConnectionService.START_INTENT_CONFIG, config);
        startService(serviceIntent);


        Intent intent = new Intent(this, NetworkSettingsActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
}
