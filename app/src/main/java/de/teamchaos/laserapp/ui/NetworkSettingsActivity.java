package de.teamchaos.laserapp.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.ServerConnectionService;

import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

public class NetworkSettingsActivity extends AppCompatActivity {

    private final static String TAG = "NetworkSettings";
    private int playerID;
    private ImageView mobile_data_led;
    private ImageView mobile_hotspot_led;
    private Timer networkLEDTimer;


    private final ActivityResultLauncher<String[]> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), isGranted -> {
                if (!isGranted.containsValue(Boolean.FALSE)) {
                    ServerConnectionService.getInstance().ifPresent(IServerConnectionService::onRuntimePermissionsGranted);
                    startGameScreen();
                } else {
                    Toast.makeText(this, "Cannot use app without location and audio", Toast.LENGTH_LONG).show();
                    ServerConnectionService.getInstance().ifPresent(IServerConnectionService::stop);
                    NetworkSettingsActivity.this.finishAndRemoveTask();
                }
            });

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.network_settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit_app:
                ServerConnectionService.getInstance().ifPresent(IServerConnectionService::stop);
                finishAndRemoveTask();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_settings);
        if (!ServerConnectionService.getInstance().isPresent()) {
            Log.w(TAG, "Service not running -> Redirect main activity");
            startService(new Intent(this, MainActivity.class));
            return;
        } else {
            playerID = ServerConnectionService.getInstance().get().getPlayerID();
        }
        Button next_button = findViewById(R.id.next_button);
        next_button.setOnClickListener(v -> {
            if (checkRuntimePermissions()) {
                startGameScreen();
            }
        });


        Button hotspot_settings_button = findViewById(R.id.hotspot_settings_button);
        hotspot_settings_button.setOnClickListener(v -> {
            Intent tetherSettings = new Intent();
            tetherSettings.setClassName("com.android.settings", "com.android.settings.TetherSettings");
            startActivity(tetherSettings);
        });

        mobile_data_led = findViewById(R.id.mobile_data_led);
        mobile_hotspot_led = findViewById(R.id.mobile_hotspot_led);

        TextView tv_ssid = findViewById(R.id.tv_ssid);
        tv_ssid.setText("Network:      MobileAP" + playerID);
        TextView tv_password = findViewById(R.id.tv_password);
        tv_password.setText("Password:    topsecret" + playerID);

        // timer to update indicator leds (red/green lights for mobile hotspot and mobile data)
        networkLEDTimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> updateNetworkLeds());

            }
        };
        networkLEDTimer.scheduleAtFixedRate(task, 0, 1000);
        this.setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

    }

    /**
     * Untested
     */
    public boolean setHotSpot(String SSID,String passWord){
        Method[] mMethods = mWifiManager.getClass().getDeclaredMethods();

        for(Method mMethod: mMethods){

            if(mMethod.getName().equals("setWifiApEnabled")) {
                WifiConfiguration netConfig = new WifiConfiguration();
                if(passWord==""){
                    netConfig.SSID = SSID;
                    netConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                    netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                    netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                }else{
                    netConfig.SSID = SSID ;
                    netConfig.preSharedKey = passWord;
                    netConfig.hiddenSSID = true;
                    netConfig.status = WifiConfiguration.Status.ENABLED;
                    netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                    netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                    netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                    netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                    netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                }
                try {
                    mMethod.invoke(mWifiManager, netConfig,false);
                    mWifiManager.saveConfiguration();
                    return true;

                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }
        return false;
    }

    private boolean checkRuntimePermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("We will sell your data to the NSA").setTitle("Explanation");
            builder.setPositiveButton("OK", (dialog, which) -> requestPermissionLauncher.launch(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO}));
            builder.setPositiveButton("Quit app", (dialog, which) -> {
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_HOME);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
            });
        } else {
            requestPermissionLauncher.launch(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO});
        }
        return false;
    }

    private void startGameScreen() {
        networkLEDTimer.cancel();
        Intent intent = ServerConnectionService.getIntentForState(this);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void updateNetworkLeds() {
        // check if mobile data enabled
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);

            if (mobileDataEnabled)
                mobile_data_led.setImageResource(R.drawable.green_circle);
            else
                mobile_data_led.setImageResource(R.drawable.red_circle);
        } catch (Exception e) {
            // Some problem accessible private API
            // TODO do whatever error handling you want here
        }

        // check if mobile hotspot running
        boolean hotspotEnabled;
        try {
            ConnectivityManager connectivtyManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            hotspotEnabled = connectivtyManager.isActiveNetworkMetered();
            if (hotspotEnabled)
                mobile_hotspot_led.setImageResource(R.drawable.green_circle);
            else
                mobile_hotspot_led.setImageResource(R.drawable.red_circle);
        } catch (Exception e) {
            return;
        }
    }
}
