package de.teamchaos.laserapp.ui;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.REFERENCE;
import de.teamchaos.laserapp.data.GameStatus;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.ServerConnectionService;
import de.teamchaos.laserapp.service.game.GameStateHandler;

public class WaitingActivity extends AppCompatActivity implements GameStateHandler.IGameStateEventListener {
    public static final String WAITING_STRING_ID_ARG = "waiting";
    private final static String TAG = "WaitingActivity";
    private TextView statusText;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.waiting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit_app:
                ServerConnectionService.getInstance().ifPresent(IServerConnectionService::stop);
                finishAndRemoveTask();
                return true;
            case R.id.continous_transmit:
                item.setChecked(!item.isChecked());
                getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).edit().putBoolean(REFERENCE.PREF_PUSH_TO_TALK, !item.isChecked()).apply();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.continous_transmit).setChecked(!getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).getBoolean(REFERENCE.PREF_PUSH_TO_TALK, true));
        return true;
    }

    @Override
    public void onStateChanged(GameStatus.State newState) {
        if (newState != GameStatus.State.SETUP) {
            ServerConnectionService.getIntentForState(this, this.getIntent()).ifPresent(this::startActivity);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_waiting);
        statusText = findViewById(R.id.status_text);

        statusText.setText(getIntent().getIntExtra(WAITING_STRING_ID_ARG, R.string.waiting_for_connection));
    }

    @Override
    protected void onPause() {
        super.onPause();
        ServerConnectionService.getInstance().ifPresent(s -> s.getGameStateHandler().unregisterListener(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServerConnectionService.getInstance().ifPresent(s -> s.getGameStateHandler().registerListener(this));
        ServerConnectionService.getInstance().filter(IServerConnectionService::isVoiceEnabled).ifPresent(s -> this.setVolumeControlStream(AudioManager.STREAM_VOICE_CALL));

    }

    @Override
    protected void onStart() {
        super.onStart();
        ServerConnectionService.getIntentForState(this, this.getIntent()).ifPresent(this::startActivity);

    }

}