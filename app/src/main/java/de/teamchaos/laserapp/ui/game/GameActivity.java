package de.teamchaos.laserapp.ui.game;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.REFERENCE;
import de.teamchaos.laserapp.data.AppConfig;
import de.teamchaos.laserapp.data.GameStatus;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.ServerConnectionService;
import de.teamchaos.laserapp.service.game.GameStateHandler;
import de.teamchaos.laserapp.service.network.AppWebsocketHandler;
import de.teamchaos.laserapp.service.network.IAppWebsocketHandler;
import de.teamchaos.laserapp.ui.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity implements GameStateHandler.IGameStateEventListener, AppWebsocketHandler.IAppWebsocketListener {
    private final static String TAG = "Game";
    private Timer serviceChecker;
    private ImageView serviceIndicatorLED;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.exit_app) {
            @Nullable
            AppConfig oldConfig = ServerConnectionService.getInstance().map(IServerConnectionService::getAppConfig).orElse(null);
            ServerConnectionService.getInstance().ifPresent(IServerConnectionService::stop);

            Intent mainActivity = new Intent(this, MainActivity.class);
            if (oldConfig != null) {
                mainActivity.putExtra(ServerConnectionService.START_INTENT_CONFIG, oldConfig);
            }

            startActivity(mainActivity);
            //Clear callstack
            finishAffinity(); // or finishAndRemoveTask();
            return true;
        } else if (itemId == R.id.continous_transmit) {
            item.setChecked(!item.isChecked());
            getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).edit().putBoolean(REFERENCE.PREF_PUSH_TO_TALK, !item.isChecked()).apply();
            return true;
        } else if (itemId == R.id.mute_audio) {
            item.setChecked(!item.isChecked());
            getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).edit().putBoolean(REFERENCE.PREF_MUTE, item.isChecked()).apply();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.mute_audio).setChecked(getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).getBoolean(REFERENCE.PREF_MUTE, false));
        menu.findItem(R.id.continous_transmit).setChecked(!getSharedPreferences(REFERENCE.PREFS, Context.MODE_PRIVATE).getBoolean(REFERENCE.PREF_PUSH_TO_TALK, true));
        return true;
    }

    @Override
    public void onStateChanged(GameStatus.State newState) {
        if (newState == GameStatus.State.SETUP) {
            ServerConnectionService.getIntentForState(this, this.getIntent()).ifPresent(this::startActivity);
        }
    }

    @Override
    public void onWebsocketConnected() {
        runOnUiThread(() -> setServiceLED(
                true
        ));
    }

    @Override
    public void onWebsocketDisconnected() {
        runOnUiThread(() -> setServiceLED(
                false
        ));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        FloatingActionButton fab = findViewById(R.id.fab);
        serviceIndicatorLED = findViewById(R.id.service_led);


    }

    @Override
    protected void onPause() {
        super.onPause();
        ServerConnectionService.getInstance().ifPresent(s -> s.getGameStateHandler().unregisterListener(this));
        ServerConnectionService.getInstance().map(IServerConnectionService::getWebsocketHandler).ifPresent(h -> h.unregisterListener(this));
        serviceChecker.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServerConnectionService.getInstance().ifPresent(s -> s.getGameStateHandler().registerListener(this));
        ServerConnectionService.getInstance().map(IServerConnectionService::getWebsocketHandler).ifPresent(h -> h.registerListener(this));
        setServiceLED(ServerConnectionService.getInstance().map(IServerConnectionService::getWebsocketHandler).map(IAppWebsocketHandler::isWebsocketConnected).orElse(false));
        serviceChecker = new Timer();
        serviceChecker.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!ServerConnectionService.getInstance().isPresent()) {
                    Log.w(TAG, "AAAAAAH, Service is gone, noooooooo");
                    startActivity(new Intent(GameActivity.this, MainActivity.class));
                }
            }
        }, 100, 5000);
        ServerConnectionService.getInstance().filter(IServerConnectionService::isVoiceEnabled).ifPresent(s -> this.setVolumeControlStream(AudioManager.STREAM_VOICE_CALL));

    }

    @Override
    protected void onStart() {
        super.onStart();
        ServerConnectionService.getIntentForState(this, this.getIntent()).ifPresent(this::startActivity);
    }

    private void setServiceLED(boolean connected) {
        if (connected) {
            serviceIndicatorLED.setImageResource(R.drawable.green_circle);
        } else {
            serviceIndicatorLED.setImageResource(R.drawable.red_circle);
        }
    }
}