package de.teamchaos.laserapp.ui.game;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.data.GameStatus;
import de.teamchaos.laserapp.data.Scoreboard;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.ServerConnectionService;
import de.teamchaos.laserapp.service.game.GameStateHandler;
import de.teamchaos.laserapp.service.game.ScoreChartData;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GameOverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GameOverviewFragment extends Fragment implements GameStateHandler.IGameStateEventListener {
    private final static String TAG = "GameOverViewFragment";

    public static GameOverviewFragment newInstance() {
        return new GameOverviewFragment();
    }

    private ArrayList<ScoreChartView> scoreCharts;
    private LinearLayout scoreBoardLayout;
    private TextView statusTextView;
    private final TimerTask timerTask_fetchAndSetTime = new TimerTask() {
        @Override
        public void run() {
            ServerConnectionService.getInstance().map(IServerConnectionService::getGameStateHandler).flatMap(GameStateHandler::getNextTimestamp).ifPresent(t -> {
                int timeLeft = (int) ((t - System.currentTimeMillis()) / 1000);
                statusTextView.setText(String.format("%d", timeLeft));
            });
        }
    };
    private int myID = -1;
    @Nullable
    private Timer gameStatusTimer;


    public GameOverviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scoreCharts = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_game_overview, container, false);
        this.scoreBoardLayout = root.findViewById(R.id.scoreboard_container);
        this.statusTextView = root.findViewById(R.id.game_state_message);
        return root;
    }

    @Override
    public void onScoreboardUpdated(ScoreChartData updatedData) {
        for (ScoreChartView view : scoreCharts) {
            int id = view.getTeam().getID();
            if (updatedData.getTeamPlayers().containsKey(id)) {
                Objects.requireNonNull(this.getActivity()).runOnUiThread(() -> view.update(updatedData.getTeamPlayers().get(id)));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ServerConnectionService.getInstance().ifPresent(s -> {
            this.myID = s.getPlayerID();
            s.getGameStateHandler().registerListener(this);
            if (s.getGameStateHandler().getCurrentState().ordinal() > GameStatus.State.SETUP.ordinal()) {
                s.getGameStateHandler().getScoreChartData().ifPresent(this::initScoreCharts);
            }
            this.onStateChanged(s.getGameStateHandler().getCurrentState());
        });
    }

    @Override
    public void onStateChanged(GameStatus.State newState) {
        if (gameStatusTimer != null) {
            gameStatusTimer.cancel();
            gameStatusTimer = null;
        }
        switch (newState) {
            case SETUP:
                statusTextView.setText("");
                break;
            case READY:
                statusTextView.setText(R.string.text_game_ready);
                break;
            case STARTING:
                gameStatusTimer = new Timer();
                gameStatusTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        statusTextView.setText(ServerConnectionService.getInstance().map(IServerConnectionService::getGameStateHandler).flatMap(GameStateHandler::getNextTimestamp).map(GameOverviewFragment.this::formatTimestamp).orElse("Starting"));
                    }
                }, 0, 1000);
                break;
            case RUNNING:
                gameStatusTimer = new Timer();
                gameStatusTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        statusTextView.setText(ServerConnectionService.getInstance().map(IServerConnectionService::getGameStateHandler).flatMap(GameStateHandler::getNextTimestamp).map(GameOverviewFragment.this::formatTimestamp).orElse("Running"));
                    }
                }, 0, 1000);

                break;
            case EVALUATION:
                statusTextView.setText("Evaluating");
                break;
            case FINISHED:
                statusTextView.setText(ServerConnectionService.getInstance().map(IServerConnectionService::getGameStateHandler).flatMap(GameStateHandler::getScoreChartData).flatMap(ScoreChartData::getLeading).map(p -> String.format(getContext().getString(R.string.winning_message), p.first, p.second)).orElse("Finished"));
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "Paused");
        ServerConnectionService.getInstance().ifPresent(s -> s.getGameStateHandler().unregisterListener(this));
    }

    private String formatTimestamp(long timeStamp) {
        long timeDiff = timeStamp - System.currentTimeMillis();
        int minutes = (int) (timeDiff / (60 * 1000));
        int seconds = (int) ((timeDiff / 1000) % 60);
        return String.format("%d:%02d", minutes, seconds);
    }

    private void initScoreCharts(ScoreChartData data) {
        if ((scoreBoardLayout).getChildCount() > 0) {
            (scoreBoardLayout).removeAllViews();
        }
        @Nullable
        Scoreboard.ScoreboardEntryData myPlayerInfo = data.getPlayers().get(myID);
        int myTeamID = myPlayerInfo != null ? myPlayerInfo.getTeam() : -1;

        scoreCharts.clear();
        data.getTeams().values().forEach(team -> {
            ScoreChartView view = new ScoreChartView(getContext(), team, myID, myTeamID);
            scoreCharts.add(view);
            scoreBoardLayout.addView(view);
        });

        onScoreboardUpdated(data);

    }


}