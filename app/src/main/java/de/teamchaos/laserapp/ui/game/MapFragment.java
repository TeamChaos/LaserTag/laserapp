package de.teamchaos.laserapp.ui.game;

import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.service.IServerConnectionService;
import de.teamchaos.laserapp.service.ServerConnectionService;
import de.teamchaos.laserapp.service.map.LocationHandler;


public class MapFragment extends Fragment implements LocationHandler.ILocationListener {

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    private TextView longitudeText;
    private TextView latitudeText;
    private TextView playerText;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_map, container, false);
        longitudeText = root.findViewById(R.id.longitude);
        latitudeText = root.findViewById(R.id.latitude);
        playerText = root.findViewById(R.id.playerid);
        return root;
    }

    @Override
    public void onLocationChanged(Location location) {
        longitudeText.setText("" + location.getLongitude());
        latitudeText.setText("" + location.getLatitude());
    }

    @Override
    public void onStart() {
        super.onStart();
        ServerConnectionService.getInstance().ifPresent(s -> {
            s.getLocationHandler().registerListener(this);
            s.getLocationHandler().getLastLocation().ifPresent(this::onLocationChanged);
            playerText.setText("Player ID " + s.getPlayerID());
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        ServerConnectionService.getInstance().map(IServerConnectionService::getLocationHandler).ifPresent(l -> l.unregisterListener(this));
    }
}