package de.teamchaos.laserapp.ui.game;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.*;
import androidx.core.content.ContextCompat;
import de.teamchaos.laserapp.R;
import de.teamchaos.laserapp.data.GameSetup;
import de.teamchaos.laserapp.data.Scoreboard;

import java.util.ArrayList;
import java.util.Collections;

public class ScoreChartView extends TableLayout {
    private final Context context;

    private final TableLayout.LayoutParams rowParams;
    private final TableRow.LayoutParams colParams;
    private final GameSetup.Team team;
    private final int myID;
    private final boolean enemy;

    public ScoreChartView(Context context, GameSetup.Team team, int myTeamID, int myID) {
        super(context);
        this.myID = myID;
        this.team = team;
        this.context = context;
        this.enemy = team.getID() == myTeamID;

        //set Emblems and name of the team
        LinearLayout emblemLayout = new LinearLayout(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        emblemLayout.setOrientation(LinearLayout.HORIZONTAL);
        emblemLayout.setLayoutParams(params);
        emblemLayout.setPadding(5, 0, 5, 0);
        // emblem
        ImageView iv = new ImageView(context);
        LinearLayout.LayoutParams ivParams = new LinearLayout.LayoutParams(150, 150);
        iv.setLayoutParams(ivParams);
        iv.setPadding(0, 0, 0, -25);
        //Team name
        TextView tv = new TextView(context);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvParams.weight = 1.0f;
        tvParams.gravity = Gravity.BOTTOM;
        tv.setPadding(5, 0, 0, 0);
        tv.setLayoutParams(tvParams);
        tv.setTextSize(19);
        tv.setTextColor(Color.WHITE);
        String teamName = team.getName();
        tv.setText(teamName);
        if (!enemy) {
            tv.setTextColor(ContextCompat.getColor(context, R.color.highlighted));
            iv.setBackgroundResource(R.drawable.granade);
        } else {
            tv.setTextColor(ContextCompat.getColor(context, R.color.white));
            iv.setBackgroundResource(R.drawable.dynamite);
        }

        //add to emblem layout
        emblemLayout.addView(iv);
        emblemLayout.addView(tv);
        addView(emblemLayout);
        //set Main Layout
        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(tableParams);
        setPadding(20, 0, 20, 30);
        setColumnStretchable(0, true);

        rowParams = new TableLayout.LayoutParams();
        colParams = new TableRow.LayoutParams();
        rowParams.setMargins(0, 0, 0, 3);
        colParams.setMargins(0, 0, 1, 0);

        //add the title bar for columns
        addColumnTitles();

    }

    public void clear() {
        //remove all views except column titles
        if (getChildCount() != 0) {
            removeViews(1, getChildCount() - 1);
        }
    }

    public GameSetup.Team getTeam() {
        return team;
    }

    public void update(ArrayList<Scoreboard.ScoreboardEntryData> players) {
        clear();
        ArrayList<Scoreboard.ScoreboardEntryData> sorted = new ArrayList<>(players);
        Collections.sort(sorted);
        for (int i = 0; i < players.size(); i++) {
            addRow(sorted.get(i));
        }

    }

    private void addColumnTitles() {
        TableRow tr = new TableRow(context);
        tr.setLayoutParams(rowParams);

        String[] columnTitles = {"Player", "Score", "Kills", "Assists", "Deaths"};
        for (String ct : columnTitles) {
            TextView tv = createTvTemplate(false);
            if (ct.equals("Player")) tv.setGravity(Gravity.LEFT);
            tv.setText(ct);
            tv.setTypeface(null, Typeface.BOLD);
            tr.addView(tv);
        }

        //add column titles to main layout
        addView(tr);
    }

    private void addRow(Scoreboard.ScoreboardEntryData p) {
        TableRow tr = new TableRow(context);
        tr.setLayoutParams(rowParams);
        if (enemy) {
            tr.setBackgroundColor(ContextCompat.getColor(context, R.color.enemyTableRow));
        } else {
            tr.setBackgroundColor(ContextCompat.getColor(context, R.color.defaultTableRow));
        }


        // if current row indicates own player stats, highlight row
        boolean highlighted = myID == Integer.valueOf(p.getID());
        //put white outline around own stats
        if (highlighted) tr.setBackgroundResource(R.drawable.highlighted_row);

        //insert players id
        TextView playerTv = createTvTemplate(highlighted);
        playerTv.setText(p.getName());
        playerTv.setGravity(Gravity.LEFT);
        tr.addView(playerTv);

        //add score
        TextView scoreTv = createTvTemplate(highlighted);
        scoreTv.setText(Integer.toString(p.getScore()));
        tr.addView(scoreTv);

        //add kills
        TextView killsTv = createTvTemplate(highlighted);
        killsTv.setText(Integer.toString(p.getKills()));
        tr.addView(killsTv);

        //add assists
        TextView assistTv = createTvTemplate(highlighted);
        assistTv.setText(Integer.toString(p.getAssists()));
        tr.addView(assistTv);

        //add deaths
        TextView deathTv = createTvTemplate(highlighted);
        deathTv.setText(Integer.toString(p.getDeaths()));
        tr.addView(deathTv);

        addView(tr);
    }

    private TextView createTvTemplate(boolean highlighted) {
        //define default Textview Template
        TextView tvTemplate = new TextView(context);
        tvTemplate.setTextSize(15);
        if (highlighted) {
            tvTemplate.setTextColor(ContextCompat.getColor(context, R.color.highlighted));
        } else {
            tvTemplate.setTextColor(Color.WHITE);
        }
        tvTemplate.setGravity(Gravity.RIGHT);
        tvTemplate.setPadding(15, 5, 15, 5);
        tvTemplate.setLayoutParams(colParams);
        return tvTemplate;
    }
}
